import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import fr.om2m.entities.ContentInstance;
import fr.om2m.exceptions.ResourceNotCreatedException;
import fr.om2m.utils.ContentInstanceConfig;
import fr.om2m.utils.HeaderSetGenerator;
import gnu.io.*;
import obix.Str;


class SerialListener implements SerialPortEventListener {

    /**
     * Handle serial events. Dispatches the event to event-specific
     * methods.
     * @param event The serial event
     */
	
    public void serialEvent(SerialPortEvent event){
    	switch(event.getEventType()) {
	    	case SerialPortEvent.DATA_AVAILABLE:
	        	SendDataToOm2m(event);
	        	break;
    	}

    }

    void SendDataToOm2m(SerialPortEvent event){
    	BufferedReader rfcommInput;
    	String response = null;
    	SerialPort port = (SerialPort) event.getSource();
    	try {
			rfcommInput = new BufferedReader(new InputStreamReader(port.getInputStream()));
	    	response = rfcommInput.readLine();
	    	
	    	addNewHeartRateValue(response);
	    	   	
		    if (rfcommInput != null){
		       	rfcommInput.close();
		    }
		} catch (IOException e) {
			e.printStackTrace();
		}    	
	}
    
    void addNewHeartRateValue(String heartRate){
		ContentInstanceConfig dataConfig = new ContentInstanceConfig();
		
		dataConfig.destinationUrl = RetrieveDataOverRfcomm.getDataContainer().getContainerUrl();
		dataConfig.contentInfo = "message";
		dataConfig.headers = HeaderSetGenerator.generateDefaultContentInstanceHeaderSet();
		
		dataConfig.content.add(new Str("category", "pulse"));
		dataConfig.content.add(new Str("data", heartRate));
		dataConfig.content.add(new Str("unit", "bpm"));
		
		try {
			ContentInstance.createContentInstanceOnRemote(dataConfig);
		} catch (ResourceNotCreatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}

