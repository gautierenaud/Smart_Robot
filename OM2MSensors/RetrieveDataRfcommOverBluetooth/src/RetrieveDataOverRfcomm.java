import gnu.io.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

import fr.om2m.entities.ApplicationEntity;
import fr.om2m.entities.Container;
import fr.om2m.entities.ContentInstance;
import fr.om2m.exceptions.ResourceNotCreatedException;
import fr.om2m.exceptions.ResourceNotFoundException;
import fr.om2m.utils.ApplicationEntityConfig;
import fr.om2m.utils.ContainerConfig;
import fr.om2m.utils.ContentInstanceConfig;
import fr.om2m.utils.HeaderSet;
import fr.om2m.utils.HeaderSetGenerator;

import obix.Str;

public class RetrieveDataOverRfcomm {
	
	private static ApplicationEntity heartRateSensorApp;
	private static Container dataContainer;
	private static Container descriptorContainer;
	private static String appId = "HeartRateSensor";
	private static String rootUrl = "http://localhost:8282/~/mn-raspi-cse/mn-raspi-name/";
    //	private static String rootUrl = "http://localhost:8080/~/mn-cse/mn-name/";
	
	public static void main(String[] args) {
		
		heartRateSensorApp = createApplication();
		descriptorContainer = createDescriptorContainer(heartRateSensorApp);
		addDescriptorInsrance(descriptorContainer);
		dataContainer = createDataContainer(heartRateSensorApp);
		
		
		String wantedPortName = "/dev/rfcomm0";
		 
		//
		// Get an enumeration of all ports known to JavaComm
		//
		Enumeration portIdentifiers = CommPortIdentifier.getPortIdentifiers();
		//
		// Check each port identifier if 
		//   (a) it indicates a serial (not a parallel) port, and
		//   (b) matches the desired name.
		//
		CommPortIdentifier portId = null;  // will be set if port found
		while (portIdentifiers.hasMoreElements())
		{
		    CommPortIdentifier pid = (CommPortIdentifier) portIdentifiers.nextElement();
		    if(pid.getPortType() == CommPortIdentifier.PORT_SERIAL &&
		       pid.getName().equals(wantedPortName)) 
		    {
		        portId = pid;
		        break;
		    }
		}
		if(portId == null)
		{
		    System.err.println("Could not find serial port " + wantedPortName);
		    System.exit(1);
		}
		//
		// Use port identifier for acquiring the port
		//
		SerialPort port = null;
		try {
		    port = (SerialPort) portId.open(
		        "name", // Name of the application asking for the port 
		        10000   // Wait max. 10 sec. to acquire port
		    );
		} catch(PortInUseException e) {
		    System.err.println("Port already in use: " + e);
		    System.exit(1);
		}
		
		//Setup Serial communication
		try {
			port.setSerialPortParams(
			    9600,
			    SerialPort.DATABITS_8,
			    SerialPort.STOPBITS_1,
			    SerialPort.PARITY_NONE);
		} catch (UnsupportedCommOperationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		  
		// Create Event Listener
		try {
			port.addEventListener(new SerialListener());
		} catch (TooManyListenersException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}		
		
		port.notifyOnDataAvailable(true);
		
		
		
		// loop waiting for user entry to exit
		promptEnterKey();
		
		// delete application
		heartRateSensorApp.removeApplicationFromRemote();
		if (port != null) port.close();
	}
	
	private static ApplicationEntity createApplication() {
		ApplicationEntityConfig heartRateSensorConfig = new ApplicationEntityConfig();
		heartRateSensorConfig.resourceName = appId;
		heartRateSensorConfig.rootUrl = rootUrl;
		
		heartRateSensorConfig.headers = HeaderSetGenerator.generateDefaultApplicationEntityHeaderSet();
		
		heartRateSensorConfig.labels.setLabel("Type", "sensor");
		heartRateSensorConfig.labels.setLabel("Location", "home");
		heartRateSensorConfig.labels.setLabel("appId", appId);
		
		ApplicationEntity application =  null;
		
		try {
			application = ApplicationEntity.createApplicationOnRemote(heartRateSensorConfig);
		} catch (ResourceNotCreatedException e) {
			System.out.println("trying retrieving application from remote");
			try {
				application = ApplicationEntity.retrieveRemoteAE(rootUrl + appId);
			} catch (ResourceNotFoundException e1) {
				e1.printStackTrace();
			}
		}
		
		return application;
	}
	
	private static Container createDescriptorContainer(ApplicationEntity parentAe) {
			ContainerConfig descriptorConfig = new ContainerConfig();
			
			descriptorConfig.resourceName = "DESCRIPTOR";
			descriptorConfig.headers = HeaderSetGenerator.generateDefaultContainerHeaderSet();
			descriptorConfig.resourceURI = parentAe.getApplicationUrl();
			
			Container descriptorContainer = null;
			
			try {
				descriptorContainer = Container.createContainerOnRemote(descriptorConfig);
			} catch (ResourceNotCreatedException e) {
				System.out.println("trying retrieving container from remote");
				try {
					descriptorContainer = Container.retrieveContainerFromRemote(new URL(parentAe.getApplicationUrl() + "/" + descriptorConfig.resourceName));
				} catch (MalformedURLException e1) {
					e1.printStackTrace();
				} catch (ResourceNotFoundException e1) {
					e1.printStackTrace();
				}
			}
			
			return descriptorContainer;
	}
	
	private static void addDescriptorInsrance(Container descriptor) {
		ContentInstanceConfig descriptionConfig = new ContentInstanceConfig();
		
		descriptionConfig.destinationUrl = descriptor.getContainerUrl();
		descriptionConfig.contentInfo = "message";
		descriptionConfig.headers = HeaderSetGenerator.generateDefaultContentInstanceHeaderSet();
		
		descriptionConfig.content.add(new Str("type", "Heart_Rate_Sensor"));
		descriptionConfig.content.add(new Str("location", "Home"));
		descriptionConfig.content.add(new Str("appId", appId));
		
		try {
			ContentInstance.createContentInstanceOnRemote(descriptionConfig);
		} catch (ResourceNotCreatedException e) {
			e.printStackTrace();
		}
	}
	
	private static Container createDataContainer(ApplicationEntity parentAe) {
		ContainerConfig dataConfig = new ContainerConfig();
		
		dataConfig.resourceName = "DATA";
		dataConfig.headers = HeaderSetGenerator.generateDefaultContainerHeaderSet();
		dataConfig.resourceURI = parentAe.getApplicationUrl();

		dataConfig.labels.setLabel("data","heart_rate");
		
		Container dataContainer = null;
		try {
			dataContainer = Container.createContainerOnRemote(dataConfig);
		} catch (ResourceNotCreatedException e) {
			System.out.println("trying retrieving container from remote");
			try {
				dataContainer = Container.retrieveContainerFromRemote(new URL(parentAe.getApplicationUrl() + "/" + dataConfig.resourceName));
			} catch (MalformedURLException e1) {
				e1.printStackTrace();
			} catch (ResourceNotFoundException e1) {
				e1.printStackTrace();
			}
		}
		
		return dataContainer;
	}
	
	public static void promptEnterKey(){
	    System.out.println("Press \"ENTER\" to kill simulator...");
	    try {
	        System.in.read();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}
	
	public static ApplicationEntity getApplicationEntity(){
		return heartRateSensorApp;
	}
	
	public static Container getDataContainer(){
		return dataContainer;
	}
	
	public static Container getDescriptorContainer(){
		return descriptorContainer;
	}
	

}
