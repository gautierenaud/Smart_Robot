package fr.om2m.utils.test;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import fr.om2m.utils.LabelSet;

public class TestLabelSet {
	
	@Test
	public void emptySetIsEmpty() {
		LabelSet testingSet = new LabelSet();
		
		Assert.assertEquals("labelSet should be empty", testingSet.isEmpty(), true);
	}

	@Test
	public void notEmptySetIsNotEmpty() {
		LabelSet testingSet = new LabelSet();
		testingSet.setLabel("Foo", "Bar");
		
		Assert.assertEquals("labelSet should not be empty", testingSet.isEmpty(), false);
	}
	
	@Test
	public void retrieveRightValues() {
		LabelSet testSet = new LabelSet();
		testSet.setLabel("Foo", "Bar");
		
		String getFoo = testSet.getLabelValue("Foo");
		String getFii = testSet.getLabelValue("Fii");
		
		Assert.assertEquals("Foo's value is Bar", getFoo, "Bar");
		Assert.assertEquals("Fii's value is null (not set)", getFii, null);
	}
	
	@Test
	public void initializeFromStringList() {
		ArrayList<String> labelList = new ArrayList<String>();
		labelList.add("Foo/Bar");
		labelList.add("Lorem/Ipsum");
		
		LabelSet testSet = new LabelSet(labelList);
		
		String getFoo = testSet.getLabelValue("Foo");
		String getLorem = testSet.getLabelValue("Lorem");

		Assert.assertEquals("Foo's value is Bar", getFoo, "Bar");
		Assert.assertEquals("Lorem's value is Ipsum", getLorem, "Ipsum");	
	}
}
