package fr.om2m.utils.test;

import org.junit.Assert;
import org.junit.Test;

import fr.om2m.utils.HeaderSet;

public class TestHeaderSet {

	@Test
	public void setThenGetHeaderValue() {
		HeaderSet testSet = new HeaderSet();
		testSet.setHeader("Foo", "Bar");
		String fooHeader = testSet.getHeaderValue("Foo");
		
		Assert.assertEquals("Foo's value should be Bar", fooHeader, "Bar");
	}

}
