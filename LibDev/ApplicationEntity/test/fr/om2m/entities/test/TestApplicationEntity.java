package fr.om2m.entities.test;

import org.junit.Assert;
import org.junit.Test;

import fr.om2m.entities.ApplicationEntity;
import fr.om2m.exceptions.ResourceNotFoundException;

public class TestApplicationEntity {

	// an IN and MN server MUST be running
	
	@Test
	public void createRemoteApplication() {
		ApplicationEntity testAE = TestHelper.createRemoteAE("testAE");
		Assert.assertNotEquals("ressource should be created", testAE, null);
		testAE.removeApplicationFromRemote();
	}
	
	@Test
	public void testRetrieveRemoteApplication() {
		String aeName = "testAE";
		ApplicationEntity originalAE = TestHelper.createRemoteAE(aeName);
		ApplicationEntity retrievedAE = null;
		try {
			retrievedAE = ApplicationEntity.retrieveRemoteAE("http://localhost:8080/~/in-cse/in-name/" + aeName);
		} catch (ResourceNotFoundException e) {
			e.printStackTrace();
		} finally {
			originalAE.removeApplicationFromRemote();
		}
		
		Assert.assertNotEquals("resource should be retrieved", null, retrievedAE);
		Assert.assertEquals("url should be the same", originalAE.getPartialUrl(), retrievedAE.getPartialUrl());
	}
}
