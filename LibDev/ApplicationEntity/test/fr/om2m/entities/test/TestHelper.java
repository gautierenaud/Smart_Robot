package fr.om2m.entities.test;

import fr.om2m.entities.ApplicationEntity;
import fr.om2m.entities.Container;
import fr.om2m.entities.ContentInstance;
import fr.om2m.exceptions.ResourceNotCreatedException;
import fr.om2m.utils.ApplicationEntityConfig;
import fr.om2m.utils.ContainerConfig;
import fr.om2m.utils.ContentInstanceConfig;
import fr.om2m.utils.HeaderSetGenerator;
import obix.Str;

public class TestHelper {

	protected static ApplicationEntity createRemoteAE(String appName) {
		ApplicationEntityConfig testConfig = new ApplicationEntityConfig();
		testConfig.rootUrl = "http://localhost:8080/~/in-cse/in-name/";
		testConfig.apiType = "api-sensor";
		testConfig.headers = HeaderSetGenerator.generateDefaultApplicationEntityHeaderSet();
		testConfig.resourceName = appName;
		
		ApplicationEntity aeToReturn = null;
		try {
			aeToReturn = ApplicationEntity.createApplicationOnRemote(testConfig);
		} catch (ResourceNotCreatedException e) {
			e.printStackTrace();
		}
		
		return aeToReturn;
	}
	
	protected static Container createRemoteCont(String contName, String destURL) {
		ContainerConfig contConfigTest = new ContainerConfig();
		contConfigTest.headers = HeaderSetGenerator.generateDefaultContainerHeaderSet();
		contConfigTest.headers.setHeader("X-M2M-Origin", "admin:admin");
		contConfigTest.resourceName = contName;
		contConfigTest.resourceURI = destURL;
		Container cont = null;
		try {
			cont = Container.createContainerOnRemote(contConfigTest);
		} catch (ResourceNotCreatedException e) {
			e.printStackTrace();
		}
		return cont;
	}
	
	protected static ContentInstance createRemoteCin(String value, String destUrl) {
		ContentInstanceConfig cinConfTest = new ContentInstanceConfig();
		cinConfTest.content.add(new Str("value", value));
		cinConfTest.headers = HeaderSetGenerator.generateDefaultContentInstanceHeaderSet();
		cinConfTest.destinationUrl = destUrl;
		
		ContentInstance cin = null;
		try {
			cin = ContentInstance.createContentInstanceOnRemote(cinConfTest);
		} catch (ResourceNotCreatedException e) {
			e.printStackTrace();
		}
		return cin;
	}
}
