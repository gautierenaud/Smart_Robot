package fr.om2m.entities.test;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Assert;
import org.junit.Test;

import fr.om2m.communication.HttpClientImpl;
import fr.om2m.communication.Response;
import fr.om2m.entities.ApplicationEntity;
import fr.om2m.entities.Container;
import fr.om2m.exceptions.ResourceNotFoundException;
import fr.om2m.utils.HeaderSetGenerator;

public class TestContainer {

	@Test
	public void testCreateContainerOnRemote() {
		String aeName = "testAE";
		String contName = "testCont";
		ApplicationEntity ae = TestHelper.createRemoteAE(aeName);
		
		Container cont = TestHelper.createRemoteCont(contName, ae.getApplicationUrl());

		Assert.assertNotEquals("ressource should be created", null, cont);
		
		ae.removeApplicationFromRemote();
	}

	@Test
	public void testRemoveContainerFromRemote() {
		String aeName = "testAE";
		String contName = "testCont";
		ApplicationEntity ae = TestHelper.createRemoteAE(aeName);
		
		Container cont = TestHelper.createRemoteCont(contName, ae.getApplicationUrl());
		cont.removeContainerFromRemote();
		
		HttpClientImpl httpClient = new HttpClientImpl();
		Response res = httpClient.retrieve(cont.getContainerUrl(), HeaderSetGenerator.generateDefaultContainerHeaderSet().getHeaderList());
		
		Assert.assertEquals("There should be no resource on the remote server", 404, res.getStatusCode());
		
		ae.removeApplicationFromRemote();
	}
	
	@Test
	public void testRetrieveContainerFromRemote() {
		String aeName = "testAE";
		String contName = "testCont";
		ApplicationEntity ae = TestHelper.createRemoteAE(aeName);
		
		Container cont = TestHelper.createRemoteCont(contName, ae.getApplicationUrl());
		Container retrievedCont = null;
		try {
			retrievedCont = Container.retrieveContainerFromRemote(new URL(cont.getContainerUrl()));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (ResourceNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println(retrievedCont.getContainerUrl());
		ae.removeApplicationFromRemote();

		Assert.assertNotEquals("ressource should be retrieved and initialized, ", null, retrievedCont);
		Assert.assertEquals("ressource should have the same name, ", retrievedCont.getInnerRepresentation().getName(), cont.getInnerRepresentation().getName());
		Assert.assertEquals("ressource should have the same Url, ", retrievedCont.getContainerUrl(), cont.getContainerUrl());	
	}
}
