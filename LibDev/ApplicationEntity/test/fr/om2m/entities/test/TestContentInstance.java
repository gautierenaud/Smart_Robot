package fr.om2m.entities.test;

import org.junit.Assert;
import org.junit.Test;

import fr.om2m.entities.ApplicationEntity;
import fr.om2m.entities.Container;
import fr.om2m.entities.ContentInstance;

public class TestContentInstance {

	@Test
	public void testCreateContentInstanceOnRemote() {
		String aeName = "testAE";
		ApplicationEntity ae = TestHelper.createRemoteAE(aeName);
		
		String contName = "testCONT";
		String contUrl = ae.getApplicationUrl();
		Container cont = TestHelper.createRemoteCont(contName, contUrl);
		
		String cinUrl = cont.getContainerUrl();
		String cinValue = "Foo";
		ContentInstance cin = TestHelper.createRemoteCin(cinValue, cinUrl);
		
		ae.removeApplicationFromRemote();
		
		Assert.assertNotEquals("cin should not be null", null, cin);
	}

}
