package gautierenaud.main;

import org.eclipse.om2m.commons.resource.AE;

import fr.om2m.entities.ApplicationEntity;
import fr.om2m.entities.Container;
import fr.om2m.entities.ContentInstance;
import fr.om2m.exceptions.ResourceNotCreatedException;
import fr.om2m.exceptions.ResourceNotFoundException;
import fr.om2m.utils.ApplicationEntityConfig;
import fr.om2m.utils.ContainerConfig;
import fr.om2m.utils.ContentInstanceConfig;
import fr.om2m.utils.HeaderSetGenerator;
import obix.Str;

public class Main {

	public static void main(String[] args) {
		// test the new version
		ApplicationEntityConfig aeConfigTest = new ApplicationEntityConfig();
		aeConfigTest.labels.setLabel("Type", "Sensor");
		aeConfigTest.labels.setLabel("Location", "Home");
		aeConfigTest.headers = HeaderSetGenerator.generateDefaultApplicationEntityHeaderSet();
		
		ApplicationEntity aeTest = null;
		try {
			aeTest = ApplicationEntity.createApplicationOnRemote(aeConfigTest);
		} catch (ResourceNotCreatedException e) {
			try {
				aeTest = ApplicationEntity.retrieveRemoteAE(aeConfigTest.rootUrl + aeConfigTest.resourceName);
			} catch (ResourceNotFoundException e1) {
				e1.printStackTrace();
			}
		}
		
		AE aeRepresentation = aeTest.getApplicationRepresentation();
		System.out.println(aeRepresentation.getAEID());
		
		ContainerConfig contConfigTest = new ContainerConfig();
		contConfigTest.headers = HeaderSetGenerator.generateDefaultContainerHeaderSet();
		contConfigTest.resourceName = "DESCRIPTOR";
		contConfigTest.resourceURI = aeTest.getApplicationUrl();
		contConfigTest.labels.setLabel("Lala", "Lolo");
		System.out.println(contConfigTest.resourceURI);
		
		Container contTest = null;
		try {
			contTest = Container.createContainerOnRemote(contConfigTest);
		} catch (ResourceNotCreatedException e) {
			e.printStackTrace();
		}
		
		ContentInstanceConfig contInstConfTest = new ContentInstanceConfig();
		contInstConfTest.content.add(new Str("type", "Temperature_Sensor"));
		contInstConfTest.content.add(new Str("location", "Home"));
		contInstConfTest.content.add(new Str("appId", "Toto"));
		contInstConfTest.destinationUrl = contTest.getContainerUrl();
		contInstConfTest.headers = HeaderSetGenerator.generateDefaultContentInstanceHeaderSet();
		
		try {
			ContentInstance.createContentInstanceOnRemote(contInstConfTest);
		} catch (ResourceNotCreatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// contTest.removeContainerFromRemote();
		aeTest.removeApplicationFromRemote();
		
		
		/*
		String appId = "MY_SENSOR";
		LabelSet labels = LabelSet.createDefaultTempSensorLabelSet();
		OM2MParametersInterface om2mParams = new DefaultOM2MParameters();
		
		Application sensorApplication = Application.createApplicationToRemoteOM2M(appId, labels, om2mParams);
		sensorApplication.addRemoteApplication();
		
		String containerName = "DESCRIPTOR";
		Container descriptorContainer = Container.createContainer(containerName);
		
		sensorApplication.addRemoteContainer(descriptorContainer);
		
		String contentInfo = "message";
		Obj descriptionInstance = new Obj();
		descriptionInstance.add(new Str("type", "Temperature_Sensor"));
		descriptionInstance.add(new Str("location", "Home"));
		descriptionInstance.add(new Str("appId", appId));
		ContentInstance dataInstance = ContentInstance.createContentInstance(contentInfo, descriptionInstance);
		
		descriptorContainer.addRemoteContentInstance(dataInstance);
		
		sensorApplication.deleteRemote();
		*/
	}

}
