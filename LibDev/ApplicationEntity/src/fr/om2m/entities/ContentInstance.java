package fr.om2m.entities;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;

import fr.om2m.communication.Header;
import fr.om2m.communication.HttpClientImpl;
import fr.om2m.communication.Response;
import fr.om2m.exceptions.ResourceNotCreatedException;
import fr.om2m.utils.ContentInstanceConfig;
import obix.io.ObixEncoder;

public class ContentInstance {
	
	private ContentInstanceConfig contInstConfig;
	
	public static ContentInstance createContentInstanceOnRemote(ContentInstanceConfig contInstConfig) throws ResourceNotCreatedException {
		return new ContentInstance(contInstConfig);
	}
	
	private ContentInstance(ContentInstanceConfig contInstConfig) throws ResourceNotCreatedException{
		this.contInstConfig = contInstConfig;
		
		createRemoteContentInstance();
	}

	private void createRemoteContentInstance() throws ResourceNotCreatedException{
		String contentInstanceXmlNotation = generateCreateContentInstanceXml();
		ArrayList<Header> createContentInstanceHeaders = generateCreateContainerHeaders();
		
		sendCreateRemoteContentInstanceRequest(contentInstanceXmlNotation, createContentInstanceHeaders);
	}
	
	private String generateCreateContentInstanceXml() {
		String contentInstanceRepresentation = "<om2m:cin xmlns:om2m=\"http://www.onem2m.org/xml/protocols\">\n";
		contentInstanceRepresentation += "<cnf>" + this.contInstConfig.contentInfo + "</cnf>\n";
		contentInstanceRepresentation += "<con>\n" + getHTMLFormattedContent() + "</con>\n";
		contentInstanceRepresentation += "</om2m:cin>";
		
		return contentInstanceRepresentation;
	}

	private String getHTMLFormattedContent() {
		String formattedContent = ObixEncoder.toString(this.contInstConfig.content);
		formattedContent = StringEscapeUtils.escapeHtml4(formattedContent);
		return formattedContent;
	}
	
	private ArrayList<Header> generateCreateContainerHeaders() {
		return this.contInstConfig.headers.getHeaderList();
	}
	
	private void sendCreateRemoteContentInstanceRequest(String contentInstanceXmlNotation, List<Header> createContentInstanceHeaders) throws ResourceNotCreatedException{
		HttpClientImpl containerClient = new HttpClientImpl();
		String remoteContainerUrl = this.contInstConfig.destinationUrl;
		
		Response createContainerResponse = containerClient.create(remoteContainerUrl, contentInstanceXmlNotation, createContentInstanceHeaders);
		if (createContainerResponse.getStatusCode() != 201) {
			throw new ResourceNotCreatedException(createContainerResponse.getRepresentation());
		}
	}
}
