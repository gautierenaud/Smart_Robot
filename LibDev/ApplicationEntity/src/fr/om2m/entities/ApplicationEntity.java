package fr.om2m.entities;

import java.util.ArrayList;

import org.eclipse.om2m.commons.resource.AE;

import fr.om2m.communication.Header;
import fr.om2m.communication.HttpClientImpl;
import fr.om2m.communication.Response;
import fr.om2m.exceptions.ResourceNotCreatedException;
import fr.om2m.exceptions.ResourceNotFoundException;
import fr.om2m.utils.ApplicationEntityConfig;
import fr.om2m.utils.HeaderSetGenerator;
import fr.om2m.utils.LabelSet;
import fr.om2m.utils.MapperImpl;

public class ApplicationEntity{

	private ApplicationEntityConfig aeConfig;
	
	private AE innerAERepresentation;
	
	private ApplicationEntity(ApplicationEntityConfig aeConfigToSet) {
		this.aeConfig = aeConfigToSet;
	}

	private ApplicationEntity(AE ae, String aeUrl) {
		this.aeConfig = new ApplicationEntityConfig();
		this.aeConfig.resourceName = ae.getName();
		this.aeConfig.apiType = ae.getAppID();
		this.aeConfig.labels = new LabelSet(ae.getLabels());
		this.aeConfig.rootUrl = aeUrl.substring(0, aeUrl.lastIndexOf("/") + 1);
		
		this.innerAERepresentation = ae;
	}
	
	public static ApplicationEntity createApplicationOnRemote(ApplicationEntityConfig aeConfigToSet) throws ResourceNotCreatedException{
		ApplicationEntity aeToReturn = new ApplicationEntity(aeConfigToSet);
		aeToReturn.createRemoteApplication();
		return aeToReturn;
	}
	
	public void removeApplicationFromRemote() {
		ArrayList<Header> deleteApplicationHeaders = generateRemoveApplicationHeaders();
		
		sendRemoveRemoteApplicationRequest(deleteApplicationHeaders);
	}
	
	private void createRemoteApplication() throws ResourceNotCreatedException{
		String applicationXmlNotation = generateCreateApplicationXml();
		ArrayList<Header> createApplicationHeaders = generateCreateApplicationHeaders();
		
		sendCreateRemoteApplicationRequest(applicationXmlNotation, createApplicationHeaders);
	}
	
	private String generateCreateApplicationXml() {
		String xmlRepresentation = "<om2m:ae xmlns:om2m=\"http://www.onem2m.org/xml/protocols\" rn=\"" + this.aeConfig.resourceName + "\">\n";
		
		xmlRepresentation += "<api>" + this.aeConfig.apiType + "</api>\n";
		
		if (!this.aeConfig.labels.isEmpty()) {
			xmlRepresentation += "<lbl>" + this.aeConfig.labels.getFormattedLabelsValues() + "</lbl>\n";
		}
		
		xmlRepresentation += "<rr>" + this.aeConfig.requestReachability + "</rr>\n";
		
		if (this.aeConfig.requestReachability) {
			xmlRepresentation += "<poa>" + this.aeConfig.poas.getFormattedPoas() + "</poa>";
		}
		
		// closing representation
		xmlRepresentation += "</om2m:ae>\n";
		
		return xmlRepresentation;
	}
	
	private ArrayList<Header> generateCreateApplicationHeaders() {
		ArrayList<Header> createAppHeaders = (ArrayList<Header>) this.aeConfig.headers.getHeaderList();
		
		if (!this.aeConfig.headers.containHeader("X-M2M-Origin")) {
			createAppHeaders.add(new Header("X-M2M-Origin", this.aeConfig.credentials));
		}
		
		return createAppHeaders;
	}
	
	private ArrayList<Header> generateRemoveApplicationHeaders() {
		ArrayList<Header> removeAppHeaders = new ArrayList<>();
		removeAppHeaders.add(new Header("X-M2M-Origin", this.aeConfig.credentials));
		return removeAppHeaders;
	}
	
	private void sendCreateRemoteApplicationRequest(String aeXmlNotationToSend, ArrayList<Header> aeHeadersToSet) throws ResourceNotCreatedException{
		HttpClientImpl applicationClient = new HttpClientImpl();
		
		Response createApplicationResponse = applicationClient.create(this.aeConfig.rootUrl, aeXmlNotationToSend, aeHeadersToSet);
		if (createApplicationResponse.getStatusCode() != 201) {
			throw new ResourceNotCreatedException(createApplicationResponse.getRepresentation());
		} else {
			MapperImpl testMapper = new MapperImpl();
			Object testObject = testMapper.unmarshal(createApplicationResponse.getRepresentation());
			this.innerAERepresentation = (AE) testObject;
		}
	}
	
	private void sendRemoveRemoteApplicationRequest(ArrayList<Header> aeHeadersToSet) {
		HttpClientImpl applicationClient = new HttpClientImpl();
		String remoteAppUrl = getApplicationUrl();
		
		Response removeApplicationResponse = applicationClient.delete(remoteAppUrl, aeHeadersToSet);
		if (removeApplicationResponse.getStatusCode() != 200) {
			System.err.println("Error during removing application, received following response : ");
			System.err.println(removeApplicationResponse);
		} else {
			System.out.println("Application removed on remote");
		}
	}
	
	public static ApplicationEntity retrieveRemoteAE(String aeURL) throws ResourceNotFoundException{
		String remoteAErepresentation = retrieveRemoteAERepresentation(aeURL);
		
		MapperImpl testMapper = new MapperImpl();
		Object testObject = testMapper.unmarshal(remoteAErepresentation);
		AE remoteAE = (AE) testObject;
		
		return new ApplicationEntity(remoteAE, aeURL);
	}
	
	private static String retrieveRemoteAERepresentation(String remoteAeURL) throws ResourceNotFoundException{
		HttpClientImpl appClient = new HttpClientImpl();
		ArrayList<Header> headers = HeaderSetGenerator.generateAdminHeaderSet().getHeaderList();
		Response retrieveResponse = appClient.retrieve(remoteAeURL, headers);
		
		if (retrieveResponse.getStatusCode() != 200) {
			throw new ResourceNotFoundException(retrieveResponse.getRepresentation());
		}
		
		return retrieveResponse.getRepresentation();
	}
	
	public String getCredentials() {
		return this.aeConfig.credentials;
	}
	
	public String getApplicationUrl() {
		return this.aeConfig.rootUrl + this.aeConfig.resourceName;
	}
	
	public String getPartialUrl() {
		return this.innerAERepresentation.getResourceID();
	}
	
	public AE getApplicationRepresentation() {
		return this.innerAERepresentation;
	}
}