package fr.om2m.entities;

import java.net.URL;
import java.util.ArrayList;

import fr.om2m.communication.Header;
import fr.om2m.communication.HttpClientImpl;
import fr.om2m.communication.Response;
import fr.om2m.exceptions.ResourceNotCreatedException;
import fr.om2m.exceptions.ResourceNotFoundException;
import fr.om2m.utils.ContainerConfig;
import fr.om2m.utils.HeaderSetGenerator;
import fr.om2m.utils.MapperImpl;

public class Container {

	private ContainerConfig contConfig;
	private org.eclipse.om2m.commons.resource.Container innerContainer;
	
	private Container(ContainerConfig contConfig) throws ResourceNotCreatedException {
		this.contConfig = contConfig;
		
		createRemoteContainer();
	}

	private Container(org.eclipse.om2m.commons.resource.Container remoteCont, URL contUrl) {
		this.innerContainer = remoteCont;
		
		this.contConfig = new ContainerConfig();
		this.contConfig.resourceName = remoteCont.getName();
		this.contConfig.resourceURI = formatUrl(contUrl, this.contConfig.resourceName);
	}
	
	private String formatUrl(URL urlToFormat, String contName) {
		String stringUrl = urlToFormat.toString();
		return stringUrl.substring(0, stringUrl.lastIndexOf("/" + contName));
	}
	
	public static Container createContainerOnRemote(ContainerConfig contConfig) throws ResourceNotCreatedException {
		Container contToCreate = new Container(contConfig);
		return contToCreate;
	}
	
	public void removeContainerFromRemote() {
		ArrayList<Header> deleteContainerHeaders = generateRemoveContainerHeaders();
		
		sendRemoveRemoteContainerRequest(deleteContainerHeaders);
	}
	
	private void createRemoteContainer() throws ResourceNotCreatedException{
		String containerXmlNotation = generateCreateContainerXml();
		ArrayList<Header> createContainerHeaders = this.contConfig.headers.getHeaderList();
		
		sendCreateRemoteContainerRequest(containerXmlNotation, createContainerHeaders);
	}
	
	private String generateCreateContainerXml() {
		String containerRepresentation = "<om2m:cnt xmlns:om2m=\"http://www.onem2m.org/xml/protocols\" rn=\"" + this.contConfig.resourceName + "\">";
		
		if (!this.contConfig.labels.isEmpty()) {
			containerRepresentation += "<lbl>" + this.contConfig.labels.getFormattedLabelsValues() + "</lbl>\n";
		}
		
		containerRepresentation += "</om2m:cnt>";
		return containerRepresentation;
	}
	
	private ArrayList<Header> generateRemoveContainerHeaders() {
		ArrayList<Header> removeContHeaders = new ArrayList<>();
		removeContHeaders.add(new Header("X-M2M-Origin", getContainerCredentials()));
		return removeContHeaders;
	}
	
	private void sendCreateRemoteContainerRequest(String contXmlNotationToSend, ArrayList<Header> contHeadersToSet) throws ResourceNotCreatedException{
		HttpClientImpl containerClient = new HttpClientImpl();
		String containerRootUrl = this.contConfig.resourceURI;
		
		Response createContainerResponse = containerClient.create(containerRootUrl, contXmlNotationToSend, contHeadersToSet);
		if (createContainerResponse.getStatusCode() != 201) {
			throw new ResourceNotCreatedException(createContainerResponse.getRepresentation());
		} else {
			MapperImpl mapper = new MapperImpl();
			Object mappedObject = mapper.unmarshal(createContainerResponse.getRepresentation());
			org.eclipse.om2m.commons.resource.Container remoteCont = (org.eclipse.om2m.commons.resource.Container) mappedObject; 
			this.innerContainer = remoteCont;
		}
	}
	
	private void sendRemoveRemoteContainerRequest(ArrayList<Header> contHeadersToSet) {
		HttpClientImpl containerClient = new HttpClientImpl();
		String containerRootUrl = this.contConfig.resourceURI + "/" + this.contConfig.resourceName;
		
		Response removeContainerResponse = containerClient.delete(containerRootUrl, contHeadersToSet);
		if (removeContainerResponse.getStatusCode() != 200) {
			System.err.println("Error during removing container, received following response : ");
			System.err.println(removeContainerResponse);
		} else {
			System.out.println("Container removed on remote");
		}
	}
	
	public static Container retrieveContainerFromRemote(URL remoteURL) throws ResourceNotFoundException{
		HttpClientImpl httpClient = new HttpClientImpl();
		Response retrieveCont = httpClient.retrieve(remoteURL.toString(), HeaderSetGenerator.generateAdminHeaderSet().getHeaderList());
		
		Container cont = null;
		if (retrieveCont.getStatusCode() != 200) {
			throw new ResourceNotFoundException(retrieveCont.getRepresentation());
		} else {
			MapperImpl mapper = new MapperImpl();
			Object mappedObject = mapper.unmarshal(retrieveCont.getRepresentation());
			org.eclipse.om2m.commons.resource.Container remoteCont = (org.eclipse.om2m.commons.resource.Container) mappedObject; 
			cont = new Container(remoteCont, remoteURL);
		}
		return cont;
	}
	
	public String getContainerUrl() {
		return this.contConfig.resourceURI + "/" + this.contConfig.resourceName;
	}
	
	public String getContainerCredentials() {
		return "admin:admin";
	}
	
	public org.eclipse.om2m.commons.resource.Container getInnerRepresentation() {
		return this.innerContainer;
	}
}
