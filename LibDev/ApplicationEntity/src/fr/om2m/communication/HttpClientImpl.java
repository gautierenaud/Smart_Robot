package fr.om2m.communication;

import java.nio.charset.Charset;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;


public class HttpClientImpl implements ClientInterface {
	
	public Response retrieve(String url, List<Header> headers) {

		CloseableHttpClient httpClient = HttpClients.createDefault();
		
		HttpGet httpGet = new HttpGet(url);
		
		for (Header headerToAdd : headers) {
			httpGet.addHeader(headerToAdd.getKey(), headerToAdd.getValue());
		}
		
		try {
			// execute the request
			CloseableHttpResponse httpResponse = httpClient.execute(httpGet);
			
			// get the received status code
			int statusCode = httpResponse.getStatusLine().getStatusCode();
			
			// get the received body
			String answer = IOUtils.toString(httpResponse.getEntity().getContent(), Charset.defaultCharset());
			
			return new Response(answer, statusCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public Response create(String url, String representation, List<Header> headers) {
		// Instanciate a http client
		CloseableHttpClient httpClient = HttpClients.createDefault();
		
		// Instanciate the correct http method
		HttpPost httpPost = new HttpPost(url);
		
		// add headers to the request
		for (Header headerToAdd : headers) {
			httpPost.addHeader(headerToAdd.getKey(), headerToAdd.getValue());
		}
		
		try {
			// add body to the request
			httpPost.setEntity(new StringEntity(representation));
			
			// execute the request
			CloseableHttpResponse httpResponse = httpClient.execute(httpPost);
			
			// get the received status code
			int statusCode = httpResponse.getStatusLine().getStatusCode();
			
			// get the received body
			String answer = IOUtils.toString(httpResponse.getEntity().getContent(), Charset.defaultCharset());
			
			return new Response(answer, statusCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

	public Response update(String url, String representation, List<Header> headers) {
		// Instanciate a http client
		CloseableHttpClient httpClient = HttpClients.createDefault();
		
		// Instanciate the correct http method
		HttpPut httpPut = new HttpPut(url);
		
		// add headers to the request
		for (Header headerToAdd : headers) {
			httpPut.addHeader(headerToAdd.getKey(), headerToAdd.getValue());
		}
		
		try {
			// add body to the request
			httpPut.setEntity(new StringEntity(representation));
			
			// execute the request
			CloseableHttpResponse httpResponse = httpClient.execute(httpPut);
			
			// get the received status code
			int statusCode = httpResponse.getStatusLine().getStatusCode();
			
			// get the received body
			String answer = IOUtils.toString(httpResponse.getEntity().getContent(), Charset.defaultCharset());
			
			return new Response(answer, statusCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

	public Response delete(String url, List<Header> headers) {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		
		HttpDelete httpDelete = new HttpDelete(url);

		for (Header headerToAdd : headers) {
			httpDelete.addHeader(headerToAdd.getKey(), headerToAdd.getValue());
		}
		
		try {
			// execute the request
			CloseableHttpResponse httpResponse = httpClient.execute(httpDelete);
			
			// get the received status code
			int statusCode = httpResponse.getStatusLine().getStatusCode();
			
			// get the received body
			String answer = IOUtils.toString(httpResponse.getEntity().getContent(), Charset.defaultCharset());
			
			return new Response(answer, statusCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
