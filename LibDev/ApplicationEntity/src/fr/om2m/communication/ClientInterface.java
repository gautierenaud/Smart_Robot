package fr.om2m.communication;

import java.util.List;

public interface ClientInterface {
	public abstract Response retrieve(String targetUrl, List<Header> headers);
	public abstract Response create(String targetUrl, String representation, List<Header> headers);
	public abstract Response update(String targetUrl, String representation, List<Header> headers);
	public abstract Response delete(String targetUrl, List<Header> headers);
}
