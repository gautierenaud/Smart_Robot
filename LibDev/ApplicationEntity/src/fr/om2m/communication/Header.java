package fr.om2m.communication;

public class Header {

	private String key;
	private String value;

	public Header(String keyToSet, String valueToSet) {
		this.key = keyToSet;
		this.value = valueToSet;
	}
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
