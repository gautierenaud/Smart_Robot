package fr.om2m.utils;

import obix.Obj;

public class ContentInstanceConfig {
	public String contentInfo = "default-message";
	public Obj content = new Obj();
	public HeaderSet headers = new HeaderSet();
	public String destinationUrl = null;
}
