package fr.om2m.utils;

public class ContainerConfig {
	public String resourceName = "MY_CONTAINER";
	public HeaderSet headers = new HeaderSet();
	public String resourceURI = null;
	public LabelSet labels = new LabelSet();
}
