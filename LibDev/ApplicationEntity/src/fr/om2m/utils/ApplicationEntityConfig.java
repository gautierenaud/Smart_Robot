package fr.om2m.utils;

public class ApplicationEntityConfig {
	public String resourceName = "MY_APP";
	public String credentials = "admin:admin";
	public String apiType = "api-default";
	public String rootUrl = "http://localhost:8080/~/mn-cse/mn-name/";
	public boolean requestReachability = false;
	public PoaSet poas = new PoaSet();
	public LabelSet labels = new LabelSet();
	public HeaderSet headers = new HeaderSet();
}
