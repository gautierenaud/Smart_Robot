package fr.om2m.utils;

import java.util.ArrayList;

public class PoaSet {
	public ArrayList<String> poaList;
	
	public PoaSet() {
		this.poaList = new ArrayList<>();
	}
	
	public String getFormattedPoas() {
		String formattedPoas = "";
		
		for (String tmpPoa : this.poaList) {
			formattedPoas += tmpPoa + " ";
		}
		
		return formattedPoas;
	}
}
