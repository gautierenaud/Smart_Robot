package fr.om2m.utils;

import java.util.List;
import java.util.Hashtable;

public class LabelSet {

	private Hashtable<String, String> labelTable;
	
	public LabelSet() {
		this.labelTable = new Hashtable<>();
	}
	
	/**
	 * converting a "key/value" formatted list of string to a hashtable of String
	 * @param labelList
	 */
	public LabelSet(List<String> labelList) {
		this.labelTable = new Hashtable<String, String>();
		for (String labelCouple : labelList) {
			String[] splittedLbl = labelCouple.split("/");
			setLabel(splittedLbl[0], splittedLbl[1]);
		}
	}
	
	public void setLabel(String labelKeyToAdd, String labelValueToAdd) {
		this.labelTable.put(labelKeyToAdd, labelValueToAdd);
	}
	
	public void removeLabel(String labelKeyToRemove) {
		this.labelTable.remove(labelKeyToRemove);
	}
	
	public String getLabelValue(String labelKeyToGet) {
		String labelValueToReturn = null;
		if (this.labelTable.containsKey(labelKeyToGet)) {
			labelValueToReturn = this.labelTable.get(labelKeyToGet);
		}
		
		return labelValueToReturn;
	}
	
	/**
	 * return the label in "key/value" format
	 * 
	 * @param labelKeyToFormat
	 * @return
	 */
	public String getFormattedLabelValue(String labelKeyToFormat) {
		String labelValueToFormat = getLabelValue(labelKeyToFormat);
		
		String formattedLabel = labelKeyToFormat + "/" + labelValueToFormat;
		
		return formattedLabel;
	}
	
	public String getFormattedLabelsValues() {
		String formattedString = "";
		
		for (String labelKey : this.labelTable.keySet()) {
			formattedString += getFormattedLabelValue(labelKey) + " ";
		}
		
		return formattedString;
	}
	
	public boolean isEmpty() {
		return this.labelTable.isEmpty();
	}
}
