package fr.om2m.utils;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.eclipse.persistence.jaxb.JAXBContextFactory;

public class MapperImpl implements MapperInterface {
	
	private Marshaller marshaller;
	private Unmarshaller unmarshaller;
	
	public MapperImpl() {
		try {
			JAXBContext jc = JAXBContextFactory.createContext(
					new Class[]{org.eclipse.om2m.commons.resource.AE.class, 
							org.eclipse.om2m.commons.resource.Container.class, 
							org.eclipse.om2m.commons.resource.ContentInstance.class}, 
					null);
			
			this.marshaller = jc.createMarshaller();
			
			this.unmarshaller = jc.createUnmarshaller();
		} catch (JAXBException e) {
			System.err.println("Error during creation");
			e.printStackTrace();
		}
	}

	@Override
	public String marshal(Object obj) {

		StringWriter marshalledResult = new StringWriter();
		try {
			this.marshaller.marshal(obj, marshalledResult);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return marshalledResult.toString();
	}

	@Override
	public Object unmarshal(String representation) {
		Object unmarshalledObject = new Object();
		try {
			unmarshalledObject = this.unmarshaller.unmarshal(new StringReader(representation));
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return unmarshalledObject;
	}

}
