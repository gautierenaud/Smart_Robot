package fr.om2m.utils;

public interface MapperInterface {
	public String marshal(Object obj);
	public Object unmarshal(String representation);
}
