package fr.om2m.utils;

public class HeaderSetGenerator {
	public static HeaderSet generateDefaultApplicationEntityHeaderSet() {
		HeaderSet defaultAeHeaderSet = new HeaderSet();
		defaultAeHeaderSet.setHeader("Content-Type", "application/xml;ty=2");
		defaultAeHeaderSet.setHeader("X-M2M-Origin", "admin:admin");
		return defaultAeHeaderSet;
	}
	
	public static HeaderSet generateDefaultContainerHeaderSet() {
		HeaderSet defaultContHeaderSet = new HeaderSet();
		defaultContHeaderSet.setHeader("Content-Type", "application/xml;ty=3");
		defaultContHeaderSet.setHeader("X-M2M-Origin", "admin:admin");
		return defaultContHeaderSet;
	}
	
	public static HeaderSet generateDefaultContentInstanceHeaderSet() {
		HeaderSet defaultCinHeaderSet = new HeaderSet();
		defaultCinHeaderSet.setHeader("Content-Type", "application/xml;ty=4");
		defaultCinHeaderSet.setHeader("X-M2M-Origin", "admin:admin");
		return defaultCinHeaderSet;
	}
	
	public static HeaderSet generateAdminHeaderSet() {
		HeaderSet defaultCinHeaderSet = new HeaderSet();
		defaultCinHeaderSet.setHeader("X-M2M-Origin", "admin:admin");
		return defaultCinHeaderSet;		
	}
}
