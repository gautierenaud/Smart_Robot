package fr.om2m.exceptions;

public class ResourceNotFoundException extends Exception {

	private static final long serialVersionUID = -7206082213294243689L;

	public ResourceNotFoundException() {}
	
	public ResourceNotFoundException(String message) {
		super(message);
	}
	
	public ResourceNotFoundException(Throwable cause) {
		super(cause);
	}
	
	public ResourceNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}
