package fr.om2m.exceptions;

public class ResourceNotCreatedException extends Exception{
	
	private static final long serialVersionUID = -1965063325273882634L;

	public ResourceNotCreatedException() {}
	
	public ResourceNotCreatedException(String message) {
		super(message);
	}
	
	public ResourceNotCreatedException(Throwable cause) {
		super(cause);
	}
	
	public ResourceNotCreatedException(String message, Throwable cause) {
		super(message, cause);
	}
}
