package fr.domoprev.main;

import java.util.Scanner;

import fr.domoprev.om2m.core.Discovery;
import fr.domoprev.om2m.core.Monitor;
import fr.domoprev.om2m.core.Subscriber;
import fr.domoprev.om2m.util.IMonitorCallBackExample;


public class Main {

	private static String url = "http://127.0.0.1:8080";
	// private static String url = "http://193.49.97.41:12033";
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String baseCseName = "in-cse";
		String label = "Robot/Commands";
		Thread monitor = new Thread(new Monitor(new IMonitorCallBackExample(), "127.0.0.1"));
		Thread discovery = new Thread(new Discovery(url,label, baseCseName));
		System.out.println("Type \"exit\" to delete all subscription and stop monitoring");
		try {
			monitor.start();
			discovery.start();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		while (true){
			String inputUser = scanLine().toLowerCase();
			if(inputUser.equalsIgnoreCase("exit")){
				System.out.println("stopping program .... ");   
				
				if(!Subscriber.isDeletedSAllSubscription()){
					System.out.println("no subscription has been done");
				}
				System.exit(0);
			}
		}
	}
	
	private static String getBaseName() {
		System.out.println("Type the base cse name : \n");
		return scanLine();
	}
	
	private static String getLabelName() {
		System.out.println("Type the label name : \n");
		return scanLine();
	}
	
	private static String scanLine() {
		Scanner scan = new Scanner(System.in);
		return scan.nextLine();
	}
}
