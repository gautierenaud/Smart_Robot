package fr.domoprev.om2m.mapper;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.eclipse.persistence.jaxb.JAXBContextFactory;


public class Mapper implements MapperInterface {

	//private static final String context = "org.eclipse.om2m.commons.resource";

	private JAXBContext ctx;
	private Unmarshaller unmarshaller;
	private Marshaller marshaller;

	public Mapper() {
		try {
			ctx = JAXBContextFactory.createContext(
					new Class[]{org.eclipse.om2m.commons.resource.Notification.class}, 
					null);
			//ctx = JAXBContext.newInstance(context);
			unmarshaller = ctx.createUnmarshaller();
			marshaller = ctx.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		} catch (JAXBException e) {
			throw new IllegalArgumentException(
					"Provided context is not correct for JAXBContext", e);
		}
	}

	@Override
	public String marshal(Object obj) {
		StringWriter sw = new StringWriter();
		String payload = null;
		// TODO
		return payload;
	}

	@Override
	public Object unmarshal(String representation) {
		Object result = null;
		InputStream repInputStream = null;
		repInputStream = new ByteArrayInputStream(representation.getBytes());
		try {
			result = unmarshaller.unmarshal(repInputStream);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// TODO
		return result;
	}

}
