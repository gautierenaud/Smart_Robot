package fr.domoprev.om2m.core;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import fr.domoprev.om2m.client.Header;
import fr.domoprev.om2m.client.HttpClientImpl;
import fr.domoprev.om2m.client.Response;


public class Discovery implements Runnable {
	
	private String baseCseName;
	private String label;
	private String credentials;
	private String baseUrl;
	
	/**
	 *  Initialize the provided baseCseName and label 
	 *  @param baseCseName, label
	 */
	public Discovery(String baseUrl, String label, String baseCseName) {
		this.baseCseName = baseCseName;
		this.label = label;
		this.credentials = "admin:admin" ;
		this.baseUrl = baseUrl;
	}
	/**
	 * Make discovery request to get all applicationEntity from OM2M
	 * @return response as ArrayList<String> 
	 */
	public ArrayList<String> sendDiscoveryRequest(){
		ArrayList<Header> discoveryHeaders = generateDiscoveryHeaders();
		ArrayList<String> appList = new ArrayList<String>() ;
		String discoveryUrl = generateDiscoveryUrl();
		if(discoveryUrl != null){
			HttpClientImpl  httpClientImpl = new HttpClientImpl();
			Response discoveryResponse = httpClientImpl.retrieve(discoveryUrl, discoveryHeaders);
			if (discoveryResponse.getStatusCode() != 200) {
				System.err.println("Error during discovery, received following response : ");
				System.err.println(discoveryResponse);
			}
			else {
				
				appList = extractUrilFromJson(discoveryResponse.getRepresentation());
			}
		}
		return appList;
	}
	
	/**
	 * Generate Discovery url 
	 * @return url as string
	 */
	private String generateDiscoveryUrl(){
		String discoveryUrl=null;
		if(!baseUrl.isEmpty() && !baseCseName.isEmpty() && !label.isEmpty()){
			discoveryUrl = baseUrl +"/~/" +baseCseName +"?fu=1&" + "lbl=" + label;
		}
		
		return discoveryUrl;
	}
	
	/**
	 * Generate Discovery headers 
	 * @return response as ArrayList<String> of headers
	 */
	private ArrayList<Header> generateDiscoveryHeaders() {
		ArrayList<Header> discoveryHeaders = new ArrayList<Header>();
		discoveryHeaders.add(new Header("X-M2M-Origin", this.credentials));
		discoveryHeaders.add(new Header("Accept", "application/json"));
		
		return discoveryHeaders;
	}
	
	/**
	 * Extract uril from the discovery response 
	 * @param String in json format
	 * @return List of uril 
	 */
	public ArrayList<String> extractUrilFromJson(String jsonInString){
		ArrayList<String> appList = new ArrayList<String>() ;
		try {
			JSONObject jsonObject = new JSONObject(jsonInString.trim());
			String discoveryUril = jsonObject.getString("m2m:uril");
			if(!discoveryUril.equals("{}")){
				for (String element : discoveryUril.split(" ")){
					appList.add(element);
				}	
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return appList ;
	}
	
	public void run(){
		while(true){
			ArrayList<String> appList = new ArrayList<String>() ;
			appList = sendDiscoveryRequest();
			if(!appList.isEmpty()){
				Subscriber subscriber = new Subscriber(baseUrl);
				subscriber.sendSouscriptionRequests(appList);
				try {
					Thread.sleep(8000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
}
