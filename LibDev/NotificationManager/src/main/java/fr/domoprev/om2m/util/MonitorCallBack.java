package fr.domoprev.om2m.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface MonitorCallBack {

	public abstract void doCallBack(HttpServletRequest req, HttpServletResponse resp);
	
}
