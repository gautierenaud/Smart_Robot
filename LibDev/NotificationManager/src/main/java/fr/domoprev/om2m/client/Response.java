package fr.domoprev.om2m.client;

public class Response {
	private String representation;
	private int statusCode;
	
	public Response(String representation, int statusCode){
		this.representation = representation;
		this.statusCode = statusCode;
	}
	
	public String toString() {
		return "Status : " + statusCode + "\n" + representation;
	}

	public String getRepresentation() {
		return representation;
	}

	public void setRepresentation(String representation) {
		this.representation = representation;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
}
