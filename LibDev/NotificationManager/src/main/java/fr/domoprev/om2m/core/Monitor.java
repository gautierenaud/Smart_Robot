package fr.domoprev.om2m.core;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.domoprev.om2m.util.MonitorCallBack;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletHandler;


public class Monitor implements Runnable {

	private static MonitorCallBack mCallBack;
	private static int PORT = 1400;
	private static String CONTEXT = "/monitor";
	private static String monitorIp;
	public static String monitorUrl;
	
	public Monitor(MonitorCallBack cb, String monitorIp) {
		mCallBack = cb;
		this.monitorIp = monitorIp;
		monitorUrl = "http://" + monitorIp + ":" + PORT + CONTEXT;
	}

	public static class MonitorServlet extends HttpServlet {

		private static final long serialVersionUID = 2302036096330714914L;

		@Override
		protected void doPost(HttpServletRequest req, HttpServletResponse resp)
				throws ServletException, IOException {
			mCallBack.doCallBack(req, resp);
		}

	}

	public void run() {
		// start the server
		Server server = new Server(PORT);
		System.out.println("new Server(PORT)");
		ServletHandler servletHandler = new ServletHandler();
		
		// add servlet and context
		servletHandler.addServletWithMapping(MonitorServlet.class, CONTEXT);
		System.out.println("servletHandler.addServletWithMapping");
		server.setHandler(servletHandler);
		System.out.println("server.setHandler");
		try {
			server.start();
			System.out.println("server.start()");
			server.join();
			System.out.println("server.join()");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
