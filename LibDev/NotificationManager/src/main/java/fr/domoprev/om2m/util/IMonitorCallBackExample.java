package fr.domoprev.om2m.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.om2m.commons.resource.Notification;

import fr.domoprev.om2m.mapper.Mapper;
import fr.domoprev.om2m.util.NotificationUtil.NotObixContentException;
import obix.Int;
import obix.Obj;
import obix.Str;

public class IMonitorCallBackExample implements MonitorCallBack {


	private static Mapper mapper = new Mapper();
	private static Obj objFromNotification = null ;
	private static Map<String, Object> valueFromNotification = new HashMap<String, Object>();
	
	@Override
	public void doCallBack(HttpServletRequest req, HttpServletResponse resp) {
		String payload = null;
		try {
			payload = getPayload(req.getReader());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("Subscription received with payload:\n"
				+ payload);

		/*
		 *  unmarshalling the notification
		 *  extracts data contains in the notification
		 */
		Notification notification = (Notification) mapper.unmarshal(payload);
		if(isTreatebleNotif(notification)){
			try {
				System.out.println("notification\n" + notification);
				objFromNotification = NotificationUtil.getObixFromNotification(notification);
				if(objFromNotification != null){
					Obj data = objFromNotification.get("data");
					Obj category = objFromNotification.get("category");
					if(category instanceof Str){
						valueFromNotification.put("category", (Str)category);
					}
					if(data instanceof Int){
						valueFromNotification.put("data", (Int)data);
					}
				}
			} catch (NotObixContentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		if(valueFromNotification.containsKey("category") && valueFromNotification.containsKey("data")){
			String sensorCategory = valueFromNotification.get("category").toString();
			int dataValue = Integer.parseInt(valueFromNotification.get("data").toString());
			System.out.println("********Notification data *********\n");
			System.out.println("Sensor : " +sensorCategory + " " +"dataValue :" + dataValue);
		}
		
		resp.setStatus(HttpServletResponse.SC_OK);
	}
	
	/**
	 * Get the payload as string
	 * 
	 * @param bufferedReader
	 * @return payload as string
	 */
	public static String getPayload(BufferedReader bufferedReader) {
		Scanner sc = new Scanner(bufferedReader);
		String payload = "";
		while (sc.hasNextLine()) {
			payload += sc.nextLine() + "\n";
		}
		sc.close();
		return payload;
	}
	
	private static boolean isTreatebleNotif(Notification notif) {
		return notif != null && notif.isVerificationRequest() != null && !notif.isVerificationRequest() && !notif.isSubscriptionDeletion();
	}
}
