package fr.gautierenaud.main;

import fr.om2m.entities.ApplicationEntity;
import fr.om2m.entities.Container;
import fr.om2m.entities.ContentInstance;
import fr.om2m.exceptions.ResourceNotCreatedException;
import fr.om2m.utils.ApplicationEntityConfig;
import fr.om2m.utils.ContainerConfig;
import fr.om2m.utils.ContentInstanceConfig;
import fr.om2m.utils.HeaderSetGenerator;
import obix.Str;

public class ProximitySimulator {
	
	private ApplicationEntity proximitySensor;
	private Container proximityDescriptor;
	private Container proximityData;
	
	private String appId;
	private String sensorLocation;
	private boolean isPersonIn;
	
	public ProximitySimulator(String appId, String location) {
		this.appId = appId;
		this.sensorLocation = location;
		
		this.isPersonIn = false;
		
		proximitySensor = createProximityApplication();
		
		proximityDescriptor = createProximityDescriptor();
		addDescription();
		
		proximityData = createProximityDataContainer();
		
	}
	
	private ApplicationEntity createProximityApplication() {
		ApplicationEntityConfig proximityAeConfig = new ApplicationEntityConfig();
		
		proximityAeConfig.headers = HeaderSetGenerator.generateDefaultApplicationEntityHeaderSet();
		proximityAeConfig.apiType = "api-simulator";
		proximityAeConfig.credentials = "admin:admin";
		proximityAeConfig.resourceName = this.appId;
		proximityAeConfig.labels.setLabel("Type", "sensor_simulator");
		proximityAeConfig.labels.setLabel("Location", this.sensorLocation);
		proximityAeConfig.labels.setLabel("appId", this.appId);
		
		ApplicationEntity ae = null;
		try {
			ae = ApplicationEntity.createApplicationOnRemote(proximityAeConfig);
		} catch (ResourceNotCreatedException e) {
			e.printStackTrace();
		}
		return ae;
	}
	
	private Container createProximityDescriptor() {
		ContainerConfig descriptorConfig = new ContainerConfig();
		descriptorConfig.resourceName = "DESCRIPTOR";
		descriptorConfig.headers = HeaderSetGenerator.generateDefaultContainerHeaderSet();
		descriptorConfig.resourceURI = this.proximitySensor.getApplicationUrl();
		Container descriptorContainer = null;
		try {
			descriptorContainer = Container.createContainerOnRemote(descriptorConfig);
		} catch (ResourceNotCreatedException e) {
			e.printStackTrace();
		}
		return descriptorContainer;
	}
	
	private void addDescription() {
		ContentInstanceConfig descriptionConfig = new ContentInstanceConfig();
		
		descriptionConfig.destinationUrl = this.proximityDescriptor.getContainerUrl();
		descriptionConfig.contentInfo = "message";
		descriptionConfig.headers = HeaderSetGenerator.generateDefaultContentInstanceHeaderSet();
		
		descriptionConfig.content.add(new Str("type", "Proximity_Sensor_Simulator"));
		descriptionConfig.content.add(new Str("location", this.sensorLocation));
		descriptionConfig.content.add(new Str("appId", this.appId));
		
		try {
			ContentInstance.createContentInstanceOnRemote(descriptionConfig);
		} catch (ResourceNotCreatedException e) {
			e.printStackTrace();
		}
	}
	
	private Container createProximityDataContainer() {
		ContainerConfig dataConfig = new ContainerConfig();
		dataConfig.resourceName = "DATA";
		dataConfig.headers = HeaderSetGenerator.generateDefaultContainerHeaderSet();
		dataConfig.resourceURI = this.proximitySensor.getApplicationUrl();
		Container dataContainer = null;
		try {
			dataContainer = Container.createContainerOnRemote(dataConfig);
		} catch (ResourceNotCreatedException e) {
			e.printStackTrace();
		}
		return dataContainer;
	}
	
	public void togglePresence() {
		this.isPersonIn = !this.isPersonIn;
		addNewProximityData();
	}
	
	private void addNewProximityData() {
		ContentInstanceConfig dataConfig = new ContentInstanceConfig();
		
		dataConfig.destinationUrl = this.proximityData.getContainerUrl();
		dataConfig.contentInfo = "message";
		dataConfig.headers = HeaderSetGenerator.generateDefaultContentInstanceHeaderSet();
		
		dataConfig.content.add(new Str("appId", "TemperatureSensorSimulator"));
		dataConfig.content.add(new Str("category", "temperature"));
		dataConfig.content.add(new obix.Bool("data", this.isPersonIn));
		dataConfig.content.add(new Str("unit", "none"));
		
		try {
			ContentInstance.createContentInstanceOnRemote(dataConfig);
		} catch (ResourceNotCreatedException e) {
			e.printStackTrace();
		}
	}
	
	public void removeRemoteApplication() {
		this.proximitySensor.removeApplicationFromRemote();
	}
}
