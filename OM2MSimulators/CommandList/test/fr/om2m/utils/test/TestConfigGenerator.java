package fr.om2m.utils.test;

import org.junit.Assert;
import org.junit.Test;

import fr.om2m.utils.ApplicationEntityConfig;
import fr.om2m.utils.ConfigGenerator;
import fr.om2m.utils.ContainerConfig;
import fr.om2m.utils.ContentInstanceConfig;

public class TestConfigGenerator {

	@Test
	public void testGetAEConfig() {
		String aeName = "testAE";
		String aeHouse = "Home";
		ApplicationEntityConfig aeConf = ConfigGenerator.getAEConfig(aeName, aeHouse);
		
		Assert.assertEquals("resource name should be setted", aeName, aeConf.resourceName);
		Assert.assertEquals("resource location should be setted", aeHouse, aeConf.labels.getLabelValue("Location"));
		Assert.assertNotEquals("resource url should be setted by default", null, aeConf.rootUrl);
		Assert.assertNotEquals("resource headers should be setted", null, aeConf.headers);
	}

	@Test
	public void testGetContConfig() {
		String contName = "testCont";
		String contUrl = "localhost";
		ContainerConfig contConf = ConfigGenerator.getContConfig(contName, contUrl);
		
		Assert.assertEquals("resource name should be setted", contName, contConf.resourceName);
		Assert.assertEquals("resource url should be setted", contUrl, contConf.resourceURI);
		Assert.assertNotEquals("resource headers should be setted", null, contConf.headers);
	}

	@Test
	public void testGetCinConfig() {
		String cinOrder = "MOVE RIGHT";
		String cinUrl = "localhost";
		ContentInstanceConfig cinConf = ConfigGenerator.getCinConfig(cinOrder, cinUrl);
		
		Assert.assertEquals("resource url should be setted", cinUrl, cinConf.destinationUrl);
		Assert.assertNotEquals("resource headers should be setted", null, cinConf.headers);
	}
}
