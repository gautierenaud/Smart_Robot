package fr.om2m.utils.test;

import org.junit.Assert;
import org.junit.Test;

import fr.om2m.entities.ApplicationEntity;
import fr.om2m.entities.Container;
import fr.om2m.utils.LabelSet;
import fr.om2m.utils.ResourceGenerator;

public class TestResourceGenerator {

	@Test
	public void testCreateApplication() {
		String aeName = "testAE";
		String sensorLocation = "Here";
		ApplicationEntity ae = ResourceGenerator.createApplication(aeName, sensorLocation);
		
		ae.removeApplicationFromRemote();
		
		Assert.assertNotEquals("ae should be initialized", null, ae);
		Assert.assertEquals("name should be the same", aeName, ae.getApplicationRepresentation().getName());
		
		LabelSet lblSet = new LabelSet(ae.getApplicationRepresentation().getLabels());
		Assert.assertEquals("location should be the same", sensorLocation, lblSet.getLabelValue("Location"));
	}
	
	@Test
	public void testCreateContainer() {
		String aeName = "testAE";
		String sensorLocation = "Here";
		ApplicationEntity ae = ResourceGenerator.createApplication(aeName, sensorLocation);
		
		String contName = "testCont";
		Container cont = ResourceGenerator.createContainer(contName, ae.getApplicationUrl());	
		
		ae.removeApplicationFromRemote();
		
		Assert.assertNotEquals("cont should be initialized", null, cont);
		Assert.assertEquals("name should be the same", contName, cont.getInnerRepresentation().getName());
	}
	
	@Test
	public void testCreateContentInstance() {
		String aeName = "testAE";
		String sensorLocation = "Here";
		ApplicationEntity ae = ResourceGenerator.createApplication(aeName, sensorLocation);
		
		String contName = "testCont";
		Container cont = ResourceGenerator.createContainer(contName, ae.getApplicationUrl());	
		
		String cinOrder = "MOVE LEFT";
		ResourceGenerator.createContentInstance(cinOrder, cont.getContainerUrl());
		
		ae.removeApplicationFromRemote();
		
		Assert.assertNotEquals("cont should be initialized", null, cont);
		Assert.assertEquals("name should be the same", contName, cont.getInnerRepresentation().getName());
	}
}
