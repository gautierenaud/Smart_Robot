package fr.om2m.utils;

import fr.domoprev.om2m.core.Discovery;
import fr.domoprev.om2m.core.Monitor;

public class HeartRateMonitor {
	
	private String url = "http://localhost:8080";
	private String heartLabel = "data/heart_rate";
	private String baseCseName = "mn-raspi-cse";
	
	public HeartRateMonitor (String orderDestUrl) {
		Thread monitor = new Thread(new Monitor(new IHeartRateCallBack(orderDestUrl), "127.0.0.1"));
		Thread discovery = new Thread(new Discovery(url, heartLabel, baseCseName));
		
		try {
			monitor.start();
			discovery.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
