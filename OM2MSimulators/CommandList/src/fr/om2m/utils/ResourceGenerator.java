package fr.om2m.utils;

import java.net.MalformedURLException;
import java.net.URL;

import fr.om2m.entities.ApplicationEntity;
import fr.om2m.entities.Container;
import fr.om2m.entities.ContentInstance;
import fr.om2m.exceptions.ResourceNotCreatedException;
import fr.om2m.exceptions.ResourceNotFoundException;
import fr.om2m.utils.ApplicationEntityConfig;
import fr.om2m.utils.ContainerConfig;
import fr.om2m.utils.ContentInstanceConfig;

public class ResourceGenerator {
	
	public static ApplicationEntity createApplication(String appId, String sensorLocation) {
		ApplicationEntityConfig aeConfig = ConfigGenerator.getAEConfig(appId, sensorLocation);
		
		ApplicationEntity ae = null;
		try {
			ae = ApplicationEntity.createApplicationOnRemote(aeConfig);
		} catch (ResourceNotCreatedException e) {
			System.out.println("trying to retrieve application from remote");
			
			String remoteAeUrl = aeConfig.rootUrl + appId;
			try {
				ae = ApplicationEntity.retrieveRemoteAE(remoteAeUrl);
			} catch (ResourceNotFoundException e1) {
				e1.printStackTrace();
			}
		}
		return ae;
	}
	
	public static Container createContainer(String contName, String contUrl) {
		ContainerConfig contConf = ConfigGenerator.getContConfig(contName, contUrl);
		
		Container cont = null;
		try {
			cont = Container.createContainerOnRemote(contConf);
		} catch (ResourceNotCreatedException e) {
			System.out.println("trying retrieving container from remote");
			try {
				cont = Container.retrieveContainerFromRemote(new URL(contUrl + "/" + contName));
			} catch (MalformedURLException e1) {
				e1.printStackTrace();
			} catch (ResourceNotFoundException e1) {
				e1.printStackTrace();
			}
		}
		return cont;
	}

	public static void createContentInstance(String orderToSend, String cinUrl) {
		ContentInstanceConfig cinConf = ConfigGenerator.getCinConfig(orderToSend, cinUrl);
		try {
			ContentInstance.createContentInstanceOnRemote(cinConf);
		} catch (ResourceNotCreatedException e) {
			e.printStackTrace();
		}
	}
}
