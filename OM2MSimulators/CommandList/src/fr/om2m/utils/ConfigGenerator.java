package fr.om2m.utils;

import fr.om2m.utils.ApplicationEntityConfig;
import fr.om2m.utils.ContainerConfig;
import fr.om2m.utils.ContentInstanceConfig;
import fr.om2m.utils.HeaderSetGenerator;
import obix.Obj;
import obix.Str;

public class ConfigGenerator {

	// private static String url = "http://193.49.97.41:12033/~/in-cse/in-name/";
	private static String url = "http://localhost:8080/~/in-cse/in-name/";
	
	public static ApplicationEntityConfig getAEConfig(String appId, String sensorLocation) {
		ApplicationEntityConfig aeConfig = new ApplicationEntityConfig();
		
		aeConfig.headers = HeaderSetGenerator.generateDefaultApplicationEntityHeaderSet();
		aeConfig.apiType = "api-simulator";
		aeConfig.credentials = "admin:admin";
		aeConfig.resourceName = appId;
		aeConfig.rootUrl = url;
		aeConfig.labels.setLabel("Type", "command_list");
		aeConfig.labels.setLabel("Location", sensorLocation);
		aeConfig.labels.setLabel("appId", appId);
		
		return aeConfig;
	}
	
	public static ContainerConfig getContConfig(String contName, String url) {
		ContainerConfig contConfig = new ContainerConfig();
		
		contConfig.headers = HeaderSetGenerator.generateDefaultContainerHeaderSet();
		contConfig.resourceName = contName;
		contConfig.resourceURI = url;
		contConfig.labels.setLabel("Robot", "Commands");
		
		return contConfig;
	}
	
	public static ContentInstanceConfig getCinConfig(String order, String url) {
		ContentInstanceConfig cinConfig = new ContentInstanceConfig();
		cinConfig.destinationUrl = url;
		cinConfig.content = new Obj();
		cinConfig.content.add(new Str("Order", order));
		cinConfig.headers = HeaderSetGenerator.generateDefaultContentInstanceHeaderSet();
		
		return cinConfig;
	}
}
