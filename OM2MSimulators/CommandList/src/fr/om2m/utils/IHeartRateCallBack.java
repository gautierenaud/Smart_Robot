package fr.om2m.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Scanner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.om2m.commons.resource.Notification;

import fr.domoprev.om2m.mapper.Mapper;
import fr.domoprev.om2m.util.MonitorCallBack;
import fr.domoprev.om2m.util.NotificationUtil;
import fr.domoprev.om2m.util.NotificationUtil.NotObixContentException;
import obix.Obj;

public class IHeartRateCallBack implements MonitorCallBack {

	private Mapper mapper = new Mapper();
	private String destUrl;
	
	public IHeartRateCallBack(String destUrl) {
		this.destUrl = destUrl;
	}
	
	public void doCallBack(HttpServletRequest req, HttpServletResponse resp) {
		String payload = getPayload(req);

		Notification notification = (Notification) mapper.unmarshal(payload);
		
		processNotification(notification);
		
		resp.setStatus(HttpServletResponse.SC_OK);
	}
	
	private void processNotification(Notification notif) {
		if(isTreatebleNotif(notif)){
			String heartBeatRate = extractHeartBeat(notif);
			int heartRateVal = Integer.parseInt(heartBeatRate);
			
			// 110 is just to test
			if (heartRateVal > 110) {
				sendOrder("Person");
			} else {
				sendOrder("Light");
			}
		}
	}
	
	private void sendOrder(String order) {
		ResourceGenerator.createContentInstance(order, this.destUrl);
	}
	
	/**
	 * Get the payload as string
	 * 
	 * @param bufferedReader
	 * @return payload as string
	 */
	private String getPayload(HttpServletRequest req) {
		Scanner sc = new Scanner(extractStream(req));
		String payload = "";
		while (sc.hasNextLine()) {
			payload += sc.nextLine() + "\n";
		}
		sc.close();
		return payload;
	}
	
	private BufferedReader extractStream(HttpServletRequest req) {
		BufferedReader buff = null;
		try {
			buff = req.getReader();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return buff;
	}
	
	
	private boolean isTreatebleNotif(Notification notif) {
		return notif != null && notif.isVerificationRequest() != null && !notif.isVerificationRequest() && !notif.isSubscriptionDeletion();
	}
	
	private String extractHeartBeat(Notification notif) {
		Obj notifObj = convertNotifToObj(notif);
		Obj heartBeat = notifObj.get("data");
		
		return heartBeat.toString();
	}
	
	private Obj convertNotifToObj(Notification notif) {
		Obj notifObj = null;
		try {
			notifObj = NotificationUtil.getObixFromNotification(notif);
		} catch (NotObixContentException e) {
			e.printStackTrace();
		}
		return notifObj;
		
	}
}
