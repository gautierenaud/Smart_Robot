package fr.om2m.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import fr.om2m.entities.ApplicationEntity;
import fr.om2m.entities.Container;

public class CmdListLauncher {

	private static String appId = "CommandList";
	private static String location = "Home";
	private static String contId = "COMMANDS";
	
	public static void main(String[] args) {
		
		ApplicationEntity ae = ResourceGenerator.createApplication(appId, location);
		Container cmdContainer = ResourceGenerator.createContainer(contId, ae.getApplicationUrl());
		
		HeartRateMonitor heartMonitor = new HeartRateMonitor(cmdContainer.getContainerUrl());
		
		/*
		String order;
		while ((order = promptEnterKey()).compareTo("exit") != 0) {
			System.out.println("sending new order : " + order);
			ResourceGenerator.createContentInstance(order, cmdContainer.getContainerUrl());
		}
		
		ae.removeApplicationFromRemote();
		*/
	}

	public static String promptEnterKey(){
	    System.out.println("Type \"exit\" to delete the remote application and exit the program, anything else to send to the robot : ");
	    try {
	    	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	    	String s = br.readLine();
	    	
	    	return s;
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    return "exit";
	}
}
