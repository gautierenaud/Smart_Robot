package fr.insa.main;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import fr.om2m.entities.ApplicationEntity;
import fr.om2m.entities.Container;
import fr.om2m.entities.ContentInstance;
import fr.om2m.exceptions.ResourceNotCreatedException;
import fr.om2m.exceptions.ResourceNotFoundException;
import fr.om2m.utils.ApplicationEntityConfig;
import fr.om2m.utils.ContainerConfig;
import fr.om2m.utils.ContentInstanceConfig;
import fr.om2m.utils.HeaderSetGenerator;
import obix.Obj;
import obix.Str;

public class TemperatureSimulator {

	private static ApplicationEntity tempSimuApp;
	private static Container dataContainer;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int minVal = 10;
		int maxVal = 40;

		tempSimuApp = createApplication();
		System.out.println(tempSimuApp.getApplicationUrl());

		// create containers
		Container descriptorContainer = createDescriptorContainer(tempSimuApp);

		addDescriptorDescription(descriptorContainer);

		dataContainer = createDataContainer(tempSimuApp);

		ContentInstanceConfig dataConfig = new ContentInstanceConfig();
		dataConfig.destinationUrl = dataContainer.getContainerUrl();
		dataConfig.headers = HeaderSetGenerator.generateDefaultContentInstanceHeaderSet();
		dataConfig.contentInfo = "message";

		// create dummy data
		for (int i = 0; i < 100; i++) {
			dataConfig.content = new Obj();
			dataConfig.content.add(new Str("appId", "TemperatureSensorSimulator"));
			dataConfig.content.add(new Str("category", "temperature"));

			// generate random data
			float randomData = minVal + (int) (Math.random() * ((maxVal - minVal) + 1));

			dataConfig.content.add(new Str("data", Float.toString(randomData)));
			dataConfig.content.add(new Str("unit", "celcius"));
			try {
				ContentInstance.createContentInstanceOnRemote(dataConfig);
			} catch (ResourceNotCreatedException e) {
				e.printStackTrace();
			}
		}

		// loop waiting for user entry to exit
		promptEnterKey();

		// delete application
		tempSimuApp.removeApplicationFromRemote();
		System.out.println("remote application deleted");
	}

	private static ApplicationEntity createApplication() {
		ApplicationEntityConfig tempSensorConfig = new ApplicationEntityConfig();
		tempSensorConfig.resourceName = "TemperatureSensorSimulator";
		tempSensorConfig.rootUrl = "http://localhost:8080/~/mn-cse/mn-name/";

		tempSensorConfig.headers = HeaderSetGenerator.generateDefaultApplicationEntityHeaderSet();

		tempSensorConfig.labels.setLabel("Type", "sensor_simulator");
		tempSensorConfig.labels.setLabel("Location", "home");
		tempSensorConfig.labels.setLabel("appId", "TemperatureSensorSimulator");

		ApplicationEntity applicationEntity = null;

		try {
			applicationEntity = ApplicationEntity.createApplicationOnRemote(tempSensorConfig);
		} catch (ResourceNotCreatedException e) {
			try {
				System.out.println("trying retrieving application from remote");
				applicationEntity = ApplicationEntity
						.retrieveRemoteAE(tempSensorConfig.rootUrl + tempSensorConfig.resourceName);
			} catch (ResourceNotFoundException e1) {
				e1.printStackTrace();
			}
		}

		return applicationEntity;
	}

	private static Container createDescriptorContainer(ApplicationEntity parentAe) {
		ContainerConfig descriptorConfig = new ContainerConfig();
		descriptorConfig.resourceName = "DESCRIPTOR";
		descriptorConfig.headers = HeaderSetGenerator.generateDefaultContainerHeaderSet();
		descriptorConfig.resourceURI = parentAe.getApplicationUrl();
		Container descriptorContainer = null;
		try {
			descriptorContainer = Container.createContainerOnRemote(descriptorConfig);
		} catch (ResourceNotCreatedException e) {
			try {
				descriptorContainer = Container.retrieveContainerFromRemote(
						new URL(parentAe.getApplicationUrl() + "/" + descriptorConfig.resourceName));
			} catch (MalformedURLException e1) {
				e1.printStackTrace();
			} catch (ResourceNotFoundException e1) {
				e1.printStackTrace();
			}
		}
		return descriptorContainer;
	}

	private static void addDescriptorDescription(Container descriptor) {
		ContentInstanceConfig descriptionConfig = new ContentInstanceConfig();

		descriptionConfig.destinationUrl = descriptor.getContainerUrl();
		descriptionConfig.contentInfo = "message";
		descriptionConfig.headers = HeaderSetGenerator.generateDefaultContentInstanceHeaderSet();

		descriptionConfig.content.add(new Str("type", "Temperature_Sensor_Simulator"));
		descriptionConfig.content.add(new Str("location", "Home"));
		descriptionConfig.content.add(new Str("appId", "TemperatureSensorSimulator"));

		try {
			ContentInstance.createContentInstanceOnRemote(descriptionConfig);
		} catch (ResourceNotCreatedException e) {
			e.printStackTrace();
		}
	}

	private static Container createDataContainer(ApplicationEntity parentAe) {
		ContainerConfig dataConfig = new ContainerConfig();
		dataConfig.resourceName = "DATA";
		dataConfig.headers = HeaderSetGenerator.generateDefaultContainerHeaderSet();
		dataConfig.resourceURI = parentAe.getApplicationUrl();
		Container dataContainer = null;
		try {
			dataContainer = Container.createContainerOnRemote(dataConfig);
		} catch (ResourceNotCreatedException e) {
			try {
				dataContainer = Container.retrieveContainerFromRemote(new URL(parentAe.getApplicationUrl() + "/" + dataConfig.resourceName));
			}
			catch (MalformedURLException e1) {
				e1.printStackTrace();
			}
			catch (ResourceNotFoundException e1){
				e1.printStackTrace();
			}
		}
		return dataContainer;
	}

	public static void promptEnterKey() {
		System.out.println("Press \"ENTER\" to kill simulator...");
		try {
			System.in.read();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
