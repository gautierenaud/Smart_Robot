package com.github.rosjava.robot_controller.turtle_controller;

import fr.domoprev.om2m.util.MonitorCallBack;
import fr.domoprev.om2m.util.NotificationUtil;
import fr.domoprev.om2m.util.NotificationUtil.NotObixContentException;
import fr.domoprev.om2m.mapper.Mapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Scanner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.om2m.commons.resource.Notification;

import obix.Obj;

public class TurtleMonitorCallBack implements MonitorCallBack {
	
	private static Mapper mapper = new Mapper();
	
	@Override
	public void doCallBack(HttpServletRequest req, HttpServletResponse resp) {
		String payload = getPayload(req);
		Notification notification = (Notification) mapper.unmarshal(payload);
		if (notification.isVerificationRequest() == null) {
			Obj extractedObj = extractObj(notification);
			Obj order = extractedObj.get("Order");
			if (order != null)
				OrderHolder.order = order.toString();
		}
		
		resp.setStatus(HttpServletResponse.SC_OK);
	}
	
	/**
	 * Get the payload as string
	 * 
	 * @param bufferedReader
	 * @return payload as string
	 */
	public static String getPayload(HttpServletRequest req) {
		Scanner sc = new Scanner(getBuffer(req));
		String payload = "";
		while (sc.hasNextLine()) {
			payload += sc.nextLine() + "\n";
		}
		sc.close();
		return payload;
	}
	
	private static BufferedReader getBuffer(HttpServletRequest req) {
		BufferedReader buff = null;
		try {
			buff = req.getReader();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return buff;
	}
	
	private static Obj extractObj(Notification notif) {
		Obj extractedObj = null;
		try {
			extractedObj = NotificationUtil.getObixFromNotification(notif);
		} catch (NotObixContentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return extractedObj;
	}
}
