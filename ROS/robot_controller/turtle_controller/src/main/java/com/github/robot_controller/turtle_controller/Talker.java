/*
 * Copyright (C) 2014 viki.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

// taken from http://guiklink.github.io/portfolio/projects/6-ROS_Java/

package com.github.rosjava.robot_controller.turtle_controller;

import org.ros.concurrent.CancellableLoop;
import org.ros.message.MessageListener;
import org.ros.namespace.GraphName;
import org.ros.node.AbstractNodeMain;
import org.ros.node.ConnectedNode;
import org.ros.node.NodeMain;
import org.ros.node.topic.Publisher;
import org.ros.node.topic.Subscriber;

import turtlesim.Pose;

import fr.domoprev.om2m.core.Discovery;
import fr.domoprev.om2m.core.Monitor;
// import fr.domoprev.om2m.core.Subscriber;

import java.util.Scanner;

/**
 * A simple {@link Publisher} {@link NodeMain}.
 */
public class Talker extends AbstractNodeMain { // Java nodes NEEDS to implement AbstractNodeMain

	Scanner scan = new Scanner(System.in);

	private PositionHolder posHolder;
	private TurtleOrderGenerator orderGen;
	
	private Point lightPos = new Point(10.0F, 10.0F);
	private Point persPos = new Point(10.0F, 1.0F);
	private Point testPos = new Point(1.0F, 7.0F);
	
	@Override
	public GraphName getDefaultNodeName() {
		return GraphName.of("rosjava/talker");
	}

	@Override
	public void onStart(final ConnectedNode connectedNode) {

		// a publisher for giving orders to turtlesim
		posHolder = new PositionHolder(connectedNode);
		final Publisher<geometry_msgs.Twist> publisher = connectedNode.newPublisher("/turtle1/cmd_vel", geometry_msgs.Twist._TYPE); // That's how you create a publisher in Java!
		this.orderGen = new TurtleOrderGenerator(posHolder, publisher);
		
		// a subscriber to get order from OM2M
		String baseUrl = "http://192.168.56.1:8080";
		String virtualMachineUrl = "192.168.56.101";
		String baseCseName = "in-cse";
		String label = "Robot/Commands";
		Thread monitor = new Thread(new Monitor(new TurtleMonitorCallBack(), virtualMachineUrl));
		Thread discovery = new Thread(new Discovery(baseUrl,label, baseCseName));
		
		fr.domoprev.om2m.core.Subscriber subscriber = new fr.domoprev.om2m.core.Subscriber(baseUrl);
		try {
			monitor.start();
			discovery.start();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// This CancellableLoop will be canceled automatically when the node shuts down.
		connectedNode.executeCancellableLoop(new CancellableLoop() {

			@Override
			protected void setup() {
			}

			@Override
			protected void loop() throws InterruptedException {
				
				boolean goal = false;
				geometry_msgs.Twist twist = null;
				
				switch (OrderHolder.order) {
					case "Light":
						twist = orderGen.generateOrder(lightPos);
						goal = true;
						break;
					case "Person":
						twist = orderGen.generateOrder(persPos);
						goal = true;
						break;
					default:
						break;
				}
				if (goal)
					publisher.publish(twist);       // Publish the message (if running use rostopic list to see the message)
				
				Thread.sleep(500);             // Sleep for 1000 ms = 1 sec
			}
		});
	}
}
