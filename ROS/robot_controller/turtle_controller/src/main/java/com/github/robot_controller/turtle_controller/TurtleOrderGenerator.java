package com.github.rosjava.robot_controller.turtle_controller;

import org.ros.node.topic.Publisher;

public class TurtleOrderGenerator {
	
	private PositionHolder posHolder;
	private Publisher<geometry_msgs.Twist> publisher;
	
	public TurtleOrderGenerator(PositionHolder posHolder, Publisher<geometry_msgs.Twist> publisher) {
		this.posHolder = posHolder;
		this.publisher = publisher;
	}
	
	public geometry_msgs.Twist generateOrder(Point destPt) {
		geometry_msgs.Twist twist = publisher.newMessage(); // Init a msg variable that of the publisher type
		
		if (this.posHolder.isInitialized() && !hasArrivedTo(destPt)) {
			// set rotation
			if (!isAlignedWith(destPt)) {
				double targetAngle = getRadAngleDiff(destPt);
				twist.getAngular().setZ(Math.signum(targetAngle) * Math.PI/8);
			} else {
				twist.getLinear().setX(1);
			}
		}
		
		return twist;
	}

	public boolean hasArrivedTo(Point destPt) {
		if (this.posHolder.isInitialized()) {
			double dist = this.posHolder.getTurtlePos().getDistanceBetween(destPt);
			return dist < 0.5;
		}
		return false;
	}
	
	public boolean isAlignedWith(Point pt) {
		if (this.posHolder.isInitialized()) {
			double angle = getRadAngleDiff(pt);
			return Math.abs(angle) < 0.2;
		}
		return false;
	}
	
	public double getRadAngleDiff(Point pt) {
		double angle = this.posHolder.getTurtlePos().getAngleBetween(pt);
		double angleDiff =  angle - this.posHolder.getTurtleAngle();
		double angleDiff2; // for when both angle points to opposite direction
		if (angleDiff < 0)
			angleDiff2 = 2 * Math.PI + angleDiff;
		else
			angleDiff2 = angleDiff - 2 * Math.PI;
		
		if (Math.abs(angleDiff) < Math.abs(angleDiff2))
			return angleDiff;
		else
			return angleDiff2;
	}
	
	public double radToDeg(double radAngle) {
		return radAngle * 180 / Math.PI;
	}
}
