package com.github.rosjava.robot_controller.turtle_controller;

import org.ros.message.MessageListener;
import org.ros.node.ConnectedNode;
import org.ros.node.topic.Subscriber;

import turtlesim.Pose;

public class PositionHolder {

	private ConnectedNode node;
	private Subscriber<turtlesim.Pose> sub;

	private Pose turtlePosition = null;
	private boolean initialized = false;
	
	public PositionHolder(ConnectedNode node) {
		this.node = node;
		
		subscribeTurtlePos();
	}
	
	private void subscribeTurtlePos() {
		this.sub = this.node.newSubscriber("/turtle1/pose", turtlesim.Pose._TYPE);
		this.sub.addMessageListener(new MessageListener<Pose>() {
			
			@Override
			public void onNewMessage(Pose pos) {				
				turtlePosition = pos;
				initialized = true;
			}
		});
	}
	
	public Point getTurtlePos() {
		Point position = new Point();
		position.x = this.turtlePosition.getX();
		position.y = this.turtlePosition.getY();
		return position;
	}
	
	public float getTurtleAngle() {
		return this.turtlePosition.getTheta();
	}
	
	public Velocity getTurtleVel() {
		Velocity vel = new Velocity();
		vel.angVel = this.turtlePosition.getAngularVelocity();
		vel.linVel = this.turtlePosition.getLinearVelocity();
		return vel;
	}
	
	public boolean isInitialized() {
		return this.initialized;
	}
}
