package com.github.rosjava.robot_controller.turtle_controller;

public class Point {
	public float x;
	public float y;
	
	public Point(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public Point() {}
	
	public double getDistanceBetween(Point pt) {
		return Math.sqrt(Math.pow(this.x - pt.x, 2) + Math.pow(this.y - pt.y, 2));
	}
	
	public double getAngleBetween(Point pt) {
		double dx = pt.x - this.x;
		double dy = pt.y - this.y;
		double theta = Math.atan2(dy, dx);
		if (theta < 0)
			theta = 2 * Math.PI + theta;
		return theta;
	}
}
