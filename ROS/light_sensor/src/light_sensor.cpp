#include "ros/ros.h"
#include "std_msgs/Bool.h"
#include <string>

using namespace std;

// the light is off by default
bool lightState = false;

template<typename T> ros::Publisher createTopic(ros::NodeHandle n, string topicName, int queueSize = 1000);
void setLightCallback(const std_msgs::Bool::ConstPtr& newLightState);

int main(int argc, char **argv) {

	ros::init(argc, argv, "lightSensor");
	ros::NodeHandle n;

	// create topic to receive order from outside (toggle light)
	ros::Publisher lightSetTopic = createTopic<std_msgs::Bool>(n, "setLight");
	// this node will subsribe to itself and use a callback function to toggle the lighe state
	ros::Subscriber lightSubscriber = n.subscribe("setLight", 1000, setLightCallback);
	// create topic to publish light status from time to time
	ros::Publisher statePub = createTopic<std_msgs::Bool>(n, "lightState", 1000);
	ros::Rate loop_rate(10);

	while (ros::ok()) {
		std_msgs::Bool msg;
		msg.data = lightState;

		statePub.publish(msg);

		// allow callback functions to be called
		ros::spinOnce();
		loop_rate.sleep();
	}
	ros::spin();
	
	return 0;
}

template<typename T>
ros::Publisher createTopic(ros::NodeHandle n, string topicName, int queueSize) {
	return n.advertise<T>(topicName, queueSize);
}

void setLightCallback(const std_msgs::Bool::ConstPtr& newLightState) {
	lightState = newLightState->data;
}
