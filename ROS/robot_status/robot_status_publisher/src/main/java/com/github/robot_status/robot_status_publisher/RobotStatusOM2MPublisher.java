package com.github.rosjava.robot_status.robot_status_publisher;

import java.net.MalformedURLException;
import java.net.URL;

import fr.om2m.entities.ApplicationEntity;
import fr.om2m.entities.Container;
import fr.om2m.entities.ContentInstance;
import fr.om2m.exceptions.ResourceNotCreatedException;
import fr.om2m.exceptions.ResourceNotFoundException;
import fr.om2m.utils.ApplicationEntityConfig;
import fr.om2m.utils.ContainerConfig;
import fr.om2m.utils.ContentInstanceConfig;
import fr.om2m.utils.HeaderSet;
import fr.om2m.utils.HeaderSetGenerator;

import turtlesim.Pose;

import obix.Str;

public class RobotStatusOM2MPublisher{
	
	private static ApplicationEntity robotStatus;
	private static Container dataContainer;
	private static Container descriptorContainer;
	private static String appId = "RobotStatus";
	private static String rootUrl = "http://localhost:8282/~/mn-roscse/mn-ros/";
	
	public static ApplicationEntity createApplication() {
		ApplicationEntityConfig robotStatusConfig = new ApplicationEntityConfig();
		robotStatusConfig.resourceName = appId;
		robotStatusConfig.rootUrl = rootUrl;
		
		robotStatusConfig.headers = HeaderSetGenerator.generateDefaultApplicationEntityHeaderSet();
		
		robotStatusConfig.labels.setLabel("Type", "sensor");
		robotStatusConfig.labels.setLabel("Location", "home");
		robotStatusConfig.labels.setLabel("appId", appId);
		
		ApplicationEntity application =  null;
		
		try {
			application = ApplicationEntity.createApplicationOnRemote(robotStatusConfig);
		} catch (ResourceNotCreatedException e) {
			System.out.println("trying retrieving application from remote");
			try {
				application = ApplicationEntity.retrieveRemoteAE(rootUrl + appId);
			} catch (ResourceNotFoundException e1) {
				e1.printStackTrace();
			}
		}
		
		return application;
	}
	
	public static Container createDescriptorContainer(ApplicationEntity parentAe) {
		ContainerConfig descriptorConfig = new ContainerConfig();
		
		descriptorConfig.resourceName = "DESCRIPTOR";
		descriptorConfig.headers = HeaderSetGenerator.generateDefaultContainerHeaderSet();
		descriptorConfig.resourceURI = parentAe.getApplicationUrl();

		Container descriptorContainer = null;
		
		try {
			descriptorContainer = Container.createContainerOnRemote(descriptorConfig);
		} catch (ResourceNotCreatedException e) {
			System.out.println("trying retrieving container from remote");
			try {
				descriptorContainer = Container.retrieveContainerFromRemote(new URL(parentAe.getApplicationUrl() + "/" + descriptorConfig.resourceName));
			} catch (MalformedURLException e1) {
				e1.printStackTrace();
			} catch (ResourceNotFoundException e1) {
				e1.printStackTrace();
			}
		}
		
		return descriptorContainer;
	}
	
	public static void addDescriptorInstance(Container descriptor) {
		ContentInstanceConfig descriptionConfig = new ContentInstanceConfig();
		
		descriptionConfig.destinationUrl = descriptor.getContainerUrl();
		descriptionConfig.contentInfo = "message";
		descriptionConfig.headers = HeaderSetGenerator.generateDefaultContentInstanceHeaderSet();
		
		descriptionConfig.content.add(new Str("type", "Robot_Status"));
		descriptionConfig.content.add(new Str("location", "Home"));
		descriptionConfig.content.add(new Str("appId", appId));
		
		try {
			ContentInstance.createContentInstanceOnRemote(descriptionConfig);
		} catch (ResourceNotCreatedException e) {
			e.printStackTrace();
		}
	}
	
	public static Container createPositionDataContainer(ApplicationEntity parentAe) {
		ContainerConfig dataConfig = new ContainerConfig();
		
		dataConfig.resourceName = "POSITION_DATA";
		dataConfig.headers = HeaderSetGenerator.generateDefaultContainerHeaderSet();
		dataConfig.resourceURI = parentAe.getApplicationUrl();

		dataConfig.labels.setLabel("robot", "status");
		dataConfig.labels.setLabel("data", "position");

		Container dataContainer = null;
		try {
			dataContainer = Container.createContainerOnRemote(dataConfig);
		} catch (ResourceNotCreatedException e) {
			System.out.println("trying retrieving container from remote");
			try {
				dataContainer = Container.retrieveContainerFromRemote(new URL(parentAe.getApplicationUrl() + "/" + dataConfig.resourceName));
			} catch (MalformedURLException e1) {
				e1.printStackTrace();
			} catch (ResourceNotFoundException e1) {
				e1.printStackTrace();
			}
		}
		
		return dataContainer;
	}
	
	public static Container createLightDataContainer(ApplicationEntity parentAe) {
		ContainerConfig dataConfig = new ContainerConfig();
		
		dataConfig.resourceName = "LIGHT_DATA";
		dataConfig.headers = HeaderSetGenerator.generateDefaultContainerHeaderSet();
		dataConfig.resourceURI = parentAe.getApplicationUrl();
		
		dataConfig.labels.setLabel("robot", "status");
		dataConfig.labels.setLabel("data", "light");

		Container dataContainer = null;
		try {
			dataContainer = Container.createContainerOnRemote(dataConfig);
		} catch (ResourceNotCreatedException e) {
			System.out.println("trying retrieving container from remote");
			try {
				dataContainer = Container.retrieveContainerFromRemote(new URL(parentAe.getApplicationUrl() + "/" + dataConfig.resourceName));
			} catch (MalformedURLException e1) {
				e1.printStackTrace();
			} catch (ResourceNotFoundException e1) {
				e1.printStackTrace();
			}
		}
		
		return dataContainer;
	}
	
	public static void addPositionDataInstance(Container data, float x, float y, float bearing) {
		ContentInstanceConfig dataConfig = new ContentInstanceConfig();
		
		dataConfig.destinationUrl = data.getContainerUrl();
		dataConfig.contentInfo = "message";
		dataConfig.headers = HeaderSetGenerator.generateDefaultContentInstanceHeaderSet();
		
		dataConfig.content.add(new Str("category", "coordinates"));
		dataConfig.content.add(new Str("x", Float.toString(x)));
		dataConfig.content.add(new Str("y", Float.toString(y)));
		dataConfig.content.add(new Str("bearing", Float.toString(bearing)));
		
		try {
			ContentInstance.createContentInstanceOnRemote(dataConfig);
		} catch (ResourceNotCreatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void addLightDataInstance(Container data, boolean light_status) {
		ContentInstanceConfig dataConfig = new ContentInstanceConfig();
		
		dataConfig.destinationUrl = data.getContainerUrl();
		dataConfig.contentInfo = "message";
		dataConfig.headers = HeaderSetGenerator.generateDefaultContentInstanceHeaderSet();
		
		dataConfig.content.add(new Str("category", "light"));
		dataConfig.content.add(new Str("Light Status", Boolean.toString(light_status)));
		
		try {
			ContentInstance.createContentInstanceOnRemote(dataConfig);
		} catch (ResourceNotCreatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
