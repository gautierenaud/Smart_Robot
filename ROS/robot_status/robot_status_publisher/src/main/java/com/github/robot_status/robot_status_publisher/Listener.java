/*
 * Copyright (C) 2014 viki.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.github.rosjava.robot_status.robot_status_publisher;

import org.apache.commons.logging.Log;
import org.ros.message.MessageListener;
import org.ros.namespace.GraphName;
import org.ros.node.AbstractNodeMain;
import org.ros.node.ConnectedNode;
import org.ros.node.NodeMain;
import org.ros.node.topic.Subscriber;

import turtlesim.Pose;

import fr.om2m.entities.ApplicationEntity;
import fr.om2m.entities.Container;

/**
 * A simple {@link Subscriber} {@link NodeMain}.
 */
public class Listener extends AbstractNodeMain {
  
  Pose previousPos =  null;

  @Override
  public GraphName getDefaultNodeName() {
    return GraphName.of("rosjava/listener");
  }

  @Override
  public void onStart(ConnectedNode connectedNode) {
    final Log log = connectedNode.getLog();  

    Subscriber<Pose> robotPositionSubscriber = connectedNode.newSubscriber("turtle1/pose", turtlesim.Pose._TYPE);

    Subscriber<std_msgs.Bool> lightStateSubscriber = connectedNode.newSubscriber("lightState", std_msgs.Bool._TYPE);
    
    ApplicationEntity app = RobotStatusOM2MPublisher.createApplication();
    
    Container descriptorContainer = RobotStatusOM2MPublisher.createDescriptorContainer(app);
    RobotStatusOM2MPublisher.addDescriptorInstance(descriptorContainer);
    
	final Container dataPositionContainer = RobotStatusOM2MPublisher.createPositionDataContainer(app);

final Container dataLightContainer = RobotStatusOM2MPublisher.createLightDataContainer(app);
	
    
    robotPositionSubscriber.addMessageListener(new MessageListener<Pose>() {
		@Override
		public void onNewMessage(Pose pos) {		
			if (shouldPoseBeSent(pos))
				RobotStatusOM2MPublisher.addPositionDataInstance(dataPositionContainer, pos.getX(), pos.getY(), pos.getTheta());
			previousPos = pos;
		}
	});
    lightStateSubscriber.addMessageListener(new MessageListener<std_msgs.Bool>() {
		@Override
		public void onNewMessage(std_msgs.Bool lightState) {				
			RobotStatusOM2MPublisher.addLightDataInstance(dataLightContainer, lightState.getData());
		}
	});
  }


  private boolean shouldPoseBeSent(Pose pos){
  	if (this.previousPos == null)
		return true;
	return isPoseEqual(pos, this.previousPos);
  }

  private boolean isPoseEqual(Pose pos, Pose oldPos){ 
  	return ((pos.getX() != oldPos.getX()) || (pos.getY() != oldPos.getY()) || (pos.getTheta() != oldPos.getTheta()) );
  }
}
