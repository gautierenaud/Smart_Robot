/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.insa.services;

import javax.ejb.Stateless;
import javax.jws.WebService;
import org.netbeans.xml.schema.authresult.AuthReplyType;
import org.netbeans.j2ee.wsdl.authenticate.authentication.AuthenticationPortType;

/**
 *
 * @author rgautier
 */
@WebService(serviceName = "AuthenticationService", portName = "AuthenticationPort", endpointInterface = "org.netbeans.j2ee.wsdl.authenticate.authentication.AuthenticationPortType", targetNamespace = "http://j2ee.netbeans.org/wsdl/Authenticate/Authentication", wsdlLocation = "META-INF/wsdl/AuthenticationService/AuthenticationWrapper.wsdl")
@Stateless
public class AuthenticationService implements AuthenticationPortType {

    public org.netbeans.xml.schema.authresult.AuthReplyType authenticate(org.netbeans.xml.schema.authcouple.AuthCoupleType authCouple) {
        AuthReplyType resultReply = new AuthReplyType();

        String loginToTest = authCouple.getLogin();
        String pwdToTest = authCouple.getPassword();

        resultReply.setAuthResult(isRightLogin(loginToTest) && isRightPwd(pwdToTest));

        return resultReply;
    }

    private boolean isRightLogin(String loginToTest) {
        return loginToTest.compareTo("admin") == 0;
    }

    private boolean isRightPwd(String pwdToTest) {
        return pwdToTest.compareTo("admin") == 0;
    }

}
