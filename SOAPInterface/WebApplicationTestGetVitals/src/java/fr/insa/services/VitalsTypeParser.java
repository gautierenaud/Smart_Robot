/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.insa.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import obix.Obj;
import obix.Str;
import obix.io.ObixDecoder;

/**
 *
 * @author kaveena
 */
public class VitalsTypeParser {

    public static ArrayList<DataCouple> formatString(String objStringlist) {
        ArrayList<String> splitObjString = splitObjList(objStringlist);
        ArrayList<DataCouple> listDataCouple = objStrListToDataCoupleList(splitObjString);
        return listDataCouple;
    }

    private static ArrayList<DataCouple> objStrListToDataCoupleList(List<String> objStringList){
        ArrayList<DataCouple> listDataCouple = new ArrayList<DataCouple>();
        for (String stringObj : objStringList){
            Obj objectObj = ObixDecoder.fromString(stringObj);
            DataCouple dataCoupleObj = objToDataCouple(objectObj);
            listDataCouple.add(dataCoupleObj);
        }
        return listDataCouple;
    }
    private static ArrayList<String> splitObjList(String xmlData){
        String[] splitData = xmlData.split("<split/>");
        ArrayList<String> splitDataArrayList = new ArrayList<String>(Arrays.asList(splitData));
        return splitDataArrayList;
    }

    private static DataCouple objToDataCouple(Obj obj) {
        DataCouple dataCoupleObj = new DataCouple();
        Obj data = obj.get("data");
        Obj unit = obj.get("unit");
        dataCoupleObj.dataValues.put("data", data.toString());
        dataCoupleObj.dataValues.put("unit", unit.toString());
        return dataCoupleObj;
    }
}
