<%-- 
    Document   : index
    Created on : Dec 28, 2016, 9:35:46 PM
    Author     : kaveena
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Get Vitals: Temperature</h1>
    </body>    <%-- start web service invocation --%><hr/>
    <%
    try {
	compositeapp1.CompositeApp1Service1 service = new compositeapp1.CompositeApp1Service1();
	compositeapp1.GetVitalsPortType port = service.getCasaPort1();

	org.netbeans.xml.schema.authcouple.AuthCoupleType authCouple = new org.netbeans.xml.schema.authcouple.AuthCoupleType();

        out.println("Credentials: admin admin");
        authCouple.setLogin("admin");
        authCouple.setPassword("admin");

	org.netbeans.xml.schema.vitals.VitalsType result = port.getVitals(authCouple);

         java.util.ArrayList<fr.insa.services.DataCouple> dataCoupleList = fr.insa.services.VitalsTypeParser.formatString(result.getData());
        for (fr.insa.services.DataCouple dataCouple : dataCoupleList) {
            out.println("data : " + dataCouple.dataValues.get("data") + ", " + dataCouple.dataValues.get("unit"));
             System.out.println("data : " + dataCouple.dataValues.get("data") + ", " + dataCouple.dataValues.get("unit"));
        }
    } catch (Exception ex) {
        out.println("Fault");
        out.println(ex.getMessage());
        ex.printStackTrace();
    }
    %> 
    <%-- end web service invocation --%><hr/>

</html>
