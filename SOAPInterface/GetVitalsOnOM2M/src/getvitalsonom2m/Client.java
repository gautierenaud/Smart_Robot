/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package getvitalsonom2m;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import obix.Int;
import obix.Obj;
import obix.Str;
import obix.io.ObixDecoder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.commons.io.IOUtils;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;


/**
 *
 * @author mamadountjoberthe
 */
public class Client {
    private static final String BASE_URL = "http://127.0.0.1:8080/~/mn-cse/mn-name";
    private static final String ORIGINATOR = "admin:admin";
    private static final String AE_NAME = "APP_OPENSTACK";
    private static final String CNT_NAME = "DATA/?rcn=4";


    public void Client(){
    }
    public String getData(){
        String response = "" ;
        try {
            CloseableHttpClient hc = HttpClients.createDefault();
            CloseableHttpResponse resp;
            HttpGet httpGet = new HttpGet(BASE_URL + "/" + AE_NAME + "/" + CNT_NAME);
            httpGet.setHeader("X-M2M-Origin", ORIGINATOR);
            httpGet.setHeader("Content-Type", " application/xml");
            resp = hc.execute(httpGet);
            System.out.println("status code :" +resp.getStatusLine().getStatusCode());
            String xmlString = IOUtils.toString(resp.getEntity().getContent(),Charset.defaultCharset());
            System.out.println("response :" + xmlString);
            response = parserXml(xmlString);

        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response ;
    }
    public String parserXml(String xml){
        System.out.println("start  parser");
        SAXBuilder builder = new SAXBuilder();
        String stringResult = "" ;

        try {
            Namespace ns = Namespace.getNamespace("http://www.onem2m.org/xml/protocols");
            InputStream stream = new ByteArrayInputStream(xml.getBytes("UTF-8"));
            Document document = (Document) builder.build(stream);
            Element rootNode = document.getRootElement();
            List list = rootNode.getChildren("cin",ns);
            System.out.println("taille list =" +list.size());
            for (int i = 0; i < list.size(); i++) {
               Element node = (Element) list.get(i);
               stringResult = stringResult + node.getChildText("con") + "<split/>";
                }
            System.out.println("response =" +stringResult);

        } catch (JDOMException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
        return stringResult;
    }
    public  void processOBJData(String stringObj){

        Obj objectObj = ObixDecoder.fromString(stringObj);
        Obj data = objectObj.get("data");
        Obj category = objectObj.get("category");
        if(category instanceof Str){
            System.out.println("Category :" + (Str)category);
        }
        if(data instanceof Int){
            System.out.println("Data :" + (Int)data);
        }
    }


}
