/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.insa.classes;

import fr.v2.om2m.entities.ApplicationEntity;
import fr.v2.om2m.entities.Container;

/**
 *
 * @author gautierenaud
 */
public class OrderSender {

    private static OrderSender instance = null;
    
    private ApplicationEntity orderSenderApp;
    private Container descriptionCont;

    public static OrderSender getInstance() {
        if (instance == null) {
            instance = new OrderSender();
        }
        return instance;
    }

    private OrderSender() {
    }
}
