/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.insa.services;

import javax.ejb.Stateless;
import javax.jws.WebService;
import org.netbeans.xml.schema.vitals.VitalsType;
import org.netbeans.j2ee.wsdl.getvitalsfromom2m.getvitalsfromom2m.GetVitalsFromOM2MPortType;

/**
 *
 * @author kaveena
 */
@WebService(serviceName = "GetVitalsFromOM2MService", portName = "GetVitalsFromOM2MPort", endpointInterface = "org.netbeans.j2ee.wsdl.getvitalsfromom2m.getvitalsfromom2m.GetVitalsFromOM2MPortType", targetNamespace = "http://j2ee.netbeans.org/wsdl/GetVitalsFromOM2M/GetVitalsFromOM2M", wsdlLocation = "META-INF/wsdl/GetVitalsFromOM2M/GetVitalsFromOM2MWrapper.wsdl")
@Stateless
public class GetVitalsFromOM2M implements GetVitalsFromOM2MPortType {

     /**
     * data is retrieved from OM2M and set as response
     * to the client
     */

    String data = "" ;
    public org.netbeans.xml.schema.vitals.VitalsType getVitalsFromOM2M(org.netbeans.xml.schema.vitals.VitalsType dummy) {
  
        Om2mClient client = new Om2mClient();
        org.netbeans.xml.schema.vitals.VitalsType response = new org.netbeans.xml.schema.vitals.VitalsType();
        data = client.getData();
        response.setData(data);
        return response ;
    }

}
